<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::group(["prefix" => "bill"], function(){
//         Route::get("view_all", "BillController@view_all")->name('bill.view_all');
//         Route::get("view_insert", "BillController@view_insert")->name('bill.view_insert');
//         Route::post("process_insert", "BillController@process_insert")->name('bill.process_insert');
//         Route::get("view_update/{id}", "BillController@view_update")->name('bill.view_update');
//         Route::post("process_update", "BillController@process_update")->name('bill.process_update');
//         Route::get("process_delete/{id}", "BillController@process_delete")->name('bill.process_delete');
//     });
// Route::get('view_all','API\BillController@view_all')->name('bill.view_all');
// Route::resource('bill','API\BillController');

// Route::group(["middleware" => "Cors"], function(){
Route::group(["prefix" => "bill"], function(){
	Route::get('', 'API\BillController@index');
	Route::get('/{bill_id}', 'API\BillController@show');
	Route::post('add', 'API\BillController@store');
});

Route::group(["prefix" => "course"], function(){
	Route::get('', 'API\CourseController@index');
	Route::get('/{course_id}', 'API\CourseController@show');
	Route::post('/add', 'API\CourseController@store');
});

Route::group(["prefix" => "product"], function(){
	Route::get('', 'API\ProductController@index');
	Route::get('/{product_id}', 'API\ProductController@show');
	Route::post('add', 'API\ProductController@store');
});

Route::group(["prefix" => "blog"], function(){
	Route::get('', 'API\BlogController@index');
	Route::get('/{blog_id}', 'API\BlogController@show');
	Route::post('add', 'API\BlogController@store');
});

Route::group(["prefix" => "registration"], function(){
	Route::get('', 'API\RegistrationController@index');
	Route::get('/{registration_id}', 'API\RegistrationController@show');
	Route::post('add', 'API\RegistrationController@store');
});
// });