<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => 'prevent-back-history'],function(){
    Route::get("", "AdminController@view_login")->name('view_login');
    Route::post("process_login", "AdminController@process_login")->name('process_login');
    Route::group(["middleware" => "CheckAdmin"], function(){
        Route::get("view_all", "AdminController@view_all")->name('admin.view_all');
        Route::get("view_insert", "AdminController@view_insert")->name('admin.view_insert');
        Route::post("process_insert", "AdminController@process_insert")->name('admin.process_insert');
        Route::get("view_upload/{id}", "AdminController@view_upload")->name('admin.view_upload');
        Route::post("process_upload", "AdminController@process_upload")->name('admin.process_upload');
        Route::get("view_profile/{id}", "AdminController@view_profile")->name('admin.view_profile');
        // Route::post("process_update", "AdminController@process_update")->name('admin.process_update');
        Route::get("process_delete/{id}", "AdminController@process_delete")->name('admin.process_delete');
        Route::get("logout", "AdminController@logout")->name('admin.logout');
        Route::get('send_mail','Controller@send_mail')->name('send_mail');

        Route::group(["prefix" => "course"], function(){
            Route::get("view_all", "CourseController@view_all")->name('course.view_all');
            Route::get("view_insert", "CourseController@view_insert")->name('course.view_insert');
            Route::post("process_insert", "CourseController@process_insert")->name('course.process_insert');
            Route::get("view_update/{id}", "CourseController@view_update")->name('course.view_update');
            Route::post("process_update", "CourseController@process_update")->name('course.process_update');
            Route::get("process_delete/{id}", "CourseController@process_delete")->name('course.process_delete');
        });

        Route::group(["prefix" => "timetable"], function(){
            Route::get("view_all", "TimetableController@view_all")->name('timetable.view_all');
            Route::post("process_insert", "TimetableController@process_insert")->name('timetable.process_insert');
            Route::get("process_delete/{id}", "TimetableController@process_delete")->name('timetable.process_delete');
        });

        Route::group(["prefix" => "gclass"], function(){
            Route::get("view_all", "GclassController@view_all")->name('gclass.view_all');
            Route::get("view_all/{id}", "GclassController@view_all_to_course")->name('gclass.view_all_to_course');
            Route::get("view_insert", "GclassController@view_insert")->name('gclass.view_insert');
            Route::get("view_insert/{id}", "GclassController@view_insert_to_course")->name('gclass.view_insert_to_course');
            Route::post("process_insert", "GclassController@process_insert")->name('gclass.process_insert');
            Route::get("view_update/{id}", "GclassController@view_update")->name('gclass.view_update');
            Route::post("process_update", "GclassController@process_update")->name('gclass.process_update');
            Route::get("process_delete/{id}", "GclassController@process_delete")->name('gclass.process_delete');
            Route::get("update_all", "GclassController@update_all")->name('gclass.update_all');

            Route::get("view/{id}", "GclassController@view")->name('gclass.view');
            Route::get("button/{id}","GclassController@button")->name('button');
        });

        Route::group(["prefix" => "student"], function(){
            Route::get("view_all", "StudentController@view_all")->name('student.view_all');
            Route::get("view_all/{id}", "StudentController@view_all_to_gclass")->name('student.view_all_to_gclass');
            Route::get("process_add_student_to_class/{gclass_id}/{student_id}/{student_email}", "StudentController@process_set_gclass")->name('student.process_set_gclass');
            Route::get("process_insert/{registration_id}", "StudentController@process_insert")->name('student.process_insert');
            Route::get("process_unset_gclass/{gclass_id}/{student_id}", "StudentController@process_unset_gclass")->name('student.process_unset_gclass');
            Route::get("process_delete/{student_id}", "StudentController@process_delete")->name('student.process_delete');
        });

        Route::group(["prefix" => "registration"], function(){
            Route::get("view_all", "RegistrationController@view_all")->name('registration.view_all');
            Route::get("view_insert", "RegistrationController@view_insert")->name('registration.view_insert');
            Route::post("process_insert", "RegistrationController@process_insert")->name('registration.process_insert');
            Route::get("view_update/{id}", "RegistrationController@view_update")->name('registration.view_update');
            Route::post("process_update", "RegistrationController@process_update")->name('registration.process_update');
            Route::get("process_delete/{id}", "RegistrationController@process_delete")->name('registration.process_delete');
        });

        // Route::group(["prefix" => "admin"], function(){
        //     Route::get("view_all", "RegistrationController@view_all")->name('admin.view_all');
        //     Route::get("view_insert", "RegistrationController@view_insert")->name('admin.view_insert');
        //     Route::post("process_insert", "RegistrationController@process_insert")->name('admin.process_insert');
        //     Route::get("view_update/{id}", "RegistrationController@view_update")->name('admin.view_update');
        //     Route::post("process_update", "RegistrationController@process_update")->name('admin.process_update');
        //     Route::get("process_delete/{id}", "RegistrationController@process_delete")->name('admin.process_delete');
        // });

        Route::group(["prefix" => "product"], function(){
            Route::get("view_all", "ProductController@view_all")->name('product.view_all');
            Route::get("view_insert", "ProductController@view_insert")->name('product.view_insert');
            Route::post("process_insert", "ProductController@process_insert")->name('product.process_insert');
            Route::get("view_update/{id}", "ProductController@view_update")->name('product.view_update');
            Route::post("process_update", "ProductController@process_update")->name('product.process_update');
            Route::get("process_delete/{id}", "ProductController@process_delete")->name('product.process_delete');
        });

        Route::group(["prefix" => "bill"], function(){
            Route::get("view_all", "BillController@view_all")->name('bill.view_all');
            Route::get("view_insert", "BillController@view_insert")->name('bill.view_insert');
            Route::post("process_insert", "BillController@process_insert")->name('bill.process_insert');
            Route::get("view_update/{id}", "BillController@view_update")->name('bill.view_update');
            Route::post("process_update", "BillController@process_update")->name('bill.process_update');
            Route::get("process_delete/{id}", "BillController@process_delete")->name('bill.process_delete');
        });

        Route::group(["prefix" => "blog"], function(){
            Route::get("view_all", "BlogController@view_all")->name('blog.view_all');
            Route::get("view_insert", "BlogController@view_insert")->name('blog.view_insert');
            Route::post("process_insert", "BlogController@process_insert")->name('blog.process_insert');
            Route::get("view_update/{id}", "BlogController@view_update")->name('blog.view_update');
            Route::post("process_update", "BlogController@process_update")->name('blog.process_update');
            Route::get("process_delete/{id}", "BlogController@process_delete")->name('blog.process_delete');
        });
    });
});