<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>@yield('title')</title>

  <!-- Custom fonts for this template-->
  <link href="{{ asset('admin/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('admin/nunito.css') }}">

  <!-- Custom styles for this template-->
  <link href="{{ asset('admin/css/sb-admin-2.min.css') }}" rel="stylesheet">
  <link href="{{ asset('admin/css/sb-admin-2.css') }}" rel="stylesheet">

  <link rel="stylesheet" href="{{ asset('admin/css/view_profile.css') }}">
  
  <link href="{{ asset('admin/css/view_upload.css') }}" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/admin.css') }}">
  
</head>
<body id="page-top">

	<div id="wrapper">
		{{-- menu --}}
		@include('layout.menu')
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        @include('layout.header')
        <div class="container-fluid">
          @yield('content')
        </div>
      </div>
      @include('layout.footer')
    </div>
	</div> 

 <!-- Bootstrap core JavaScript-->
  <!-- Bootstrap core JavaScript-->
  
  <script src="{{ asset('admin/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('admin/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{ asset('admin/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{ asset('admin/js/sb-admin-2.min.js') }}"></script>

  <script type="text/javascript" src="{{ asset('ckeditor/ckeditor.js') }}"></script>
  <script type="text/javascript">
    CKEDITOR.replace('editor',{
      filebrowserBrowseUrl: '{{ asset('ckfinder/ckfinder.html') }}',
          filebrowserImageBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Images') }}',
          filebrowserFlashBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Flash') }}',
          filebrowserUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
          filebrowserImageUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
          filebrowserFlashUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
      })
  </script>

  <script src="{{ asset('admin/vendor/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('admin/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

  <script src="{{ asset('admin/js/demo/datatables-demo.js') }}"></script>
  <!-- Page level custom scripts -->
  {{-- <script src="{{ asset('admin/js/demo/chart-area-demo.js') }}"></script>
  <script src="{{ asset('admin/js/demo/chart-pie-demo.js') }}"></script> --}}
  
</body>
</html>