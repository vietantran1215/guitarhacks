
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
    <div class="sidebar-brand-icon rotate-n-15">
      <i class="fas fa-laugh-wink"></i>
    </div>
    <div class="sidebar-brand-text mx-3">Admin <sup>2</sup></div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  <!-- Nav Item - Dashboard -->
  <li class="nav-item active">
    <a class="nav-link" href="{{ route('admin.view_all') }}">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Member</span></a>
  </li>

  <!-- Divider -->
  <hr class="sidebar-divider">

  <!-- Heading -->
  <div class="sidebar-heading">
    Khóa học - Sản phẩm
  </div>

  <!-- Nav Item - Pages Collapse Menu -->
  <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
      <i class="fas fa-graduation-cap"></i>
      <span>Quản lý khóa học</span>
    </a>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
       
        <a class="collapse-item" href="{{ route('course.view_all') }}">Khóa học</a>
        <a class="collapse-item" href="{{ route('gclass.view_all') }}">Lớp học</a>
        
        <a class="collapse-item" href="{{ route('timetable.view_all') }}">Thời khóa biểu</a>
      </div>
    </div>
  </li>

  <!-- Nav Item - Utilities Collapse Menu -->
  <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
      <i class="fas fa-guitar" aria-hidden="true"></i>
      <span>Sản phẩm</span>
    </a>
    <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <a class="collapse-item" href="{{ route('product.view_all') }}">Sản Phẩm</a>
        <a class="collapse-item" href="{{ route('bill.view_all') }}">Hóa đơn</a>
      </div>
    </div>
  </li>

  <!-- Divider -->
  <hr class="sidebar-divider">

  <!-- Heading -->
  <div class="sidebar-heading">
    Quản lý học viên
  </div>

  <!-- Nav Item - Pages Collapse Menu -->
  <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
      <i class="fas fa-user-graduate"></i>
      <span>Danh sách</span>
    </a>
    <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <a class="collapse-item" href="{{ route('student.view_all') }}">Học viên</a>
        <a class="collapse-item" href="{{ route('registration.view_all') }}">Đơn đăng ký</a>
      </div>
    </div>
  </li>

  <!-- Divider -->
  <hr class="sidebar-divider">

  <!-- Heading -->
  <div class="sidebar-heading">
    Quản lý bài viết
  </div>

  <!-- Nav Item - Pages Collapse Menu -->
  <li class="nav-item">
     <!-- Nav Item - Charts -->
  <li class="nav-item">
    {{-- {{$admin_id = Session::get('admin_id')}} --}}
    <a class="nav-link" href="{{ route('blog.view_all',['admin_id'=> Session::get('admin_id')]) }}">
      <i class="fas fa-fw fa-folder"></i>
      <span>Bài viết</span></a>
    
  </li>

  <!-- Nav Item - Tables -->
  {{-- <li class="nav-item">
    <a class="nav-link" href="#">
      <i class="fas fa-fw fa-table"></i>
      <span>Tables</span></a>
  </li>
  </li> --}}

 

  <!-- Divider -->
  <hr class="sidebar-divider d-none d-md-block">

  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>
