<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
@extends('layout.main')
@section('content')
        <h1 class="h3 mb-0 text-gray-800">Thêm lịch</h1>
        <br>
        <center>
        <form action="{{route('timetable.process_insert')}}" method="post">
            {{csrf_field()}}
        
            <table  style="width: 50%">
                <tr>
                    <td style="width:50% ; font-weight: 600; color: #5a5c69" >
                        Ngày trong tuần
                    </td>
                   
                    <td>
                        <select name="timetable_day">
                            <option value="2">Thứ hai</option>
                            <option value="3">Thứ ba</option>
                            <option value="4">Thứ tư</option>
                            <option value="5">Thứ năm</option>
                            <option value="6">Thứ sáu</option>
                            <option value="7">Thứ bảy</option>
                            <option value="8">Chủ nhật</option>
                        </select>
                    </td>
                </tr>
                <tr>
                     <td style="font-weight: 600; color: #5a5c69">
                        Giờ
                    </td>
                    
                    </td>
                    <td>
                        <input type="time" name="timetable_time">
                    </td>
                </tr>
            </table>
            <br>
            <button type="button" class="btn btn-primary">Thêm</button>
        </form>
        </center>

        <hr>

          <h1 class="h3 mb-0 text-gray-800">Lịch</h1>
        <table class="table table-hover" >
            <tr class="tr_text" >
                <td style="text-align: center;">|Ngày|</td>
                <td style="text-align: center;">|Giờ|</td>
                <td style="text-align: center;">|Xóa|</td>
            </tr>
        @foreach($array_timetable as $timetable)
            <tr style="text-align: center;">
                <td>
                    @switch($timetable->timetable_day)
                        @case(8)
                            Chủ nhật
                            @break
                        @case(2)
                            Thứ hai
                            @break
                        @case(3)
                            Thứ ba
                            @break
                        @case(4)
                            Thứ tư
                            @break
                        @case(5)
                            Thứ năm
                            @break
                        @case(6)
                            Thứ sáu
                            @break
                        @case(7)
                            Thứ bảy
                            @break
                        @default
                            Không rõ
                            @break
                    @endswitch
                </td>
                <td>{{$timetable->timetable_time}}</td>
                <td>
                    <a href="{{route('timetable.process_delete',['id' => $timetable->timetable_id])}}" class="btn btn-danger btn-circle btn-sm">
                    <i class="fas fa-trash"></i>
                  </a>
                    {{-- <a href="{{route('timetable.process_delete',['id' => $timetable->timetable_id])}}">Xóa</a> --}}
                </td>
            </tr>
        @endforeach
        </table>
        {{$array_timetable->links()}}
 @endsection 
     </body>
</html> 
