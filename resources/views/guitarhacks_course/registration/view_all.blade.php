<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Học viên đăng ký</title>
    </head>
    <body>
        @extends('layout.main')
        @section('title')
        
        @endsection

        @section('content')
        <h1 class="h3 mb-0 text-gray-800">Danh sách đăng ký</h1>
        <a  href="{{route('registration.view_insert')}}" class="btn btn-info btn-circle btn-sm">
            <i class="fas fa-plus"></i>
        </a>
       {{--  <a href="{{route('registration.view_insert')}}">Đăng ký</a> --}}
        <table  class="table table-hover">
            <tr class="tr_text">
                <td>Họ và tên</td>
                <td>Số điện thoại</td>
                <td>Ngày sinh</td>
                <td>Ngày đăng ký</td>
                <td>Tình trạng</td>
                <td>Số buổi còn lại</td>
                <td>Khóa học</td>
                <td>Email</td>
                
            </tr>
        @foreach($array_registration as $registration)
            <tr>
                <td>{{$registration->registration_student_full_name}}</td>
                <td>{{$registration->registration_student_phone_number}}</td>
                <td>{{$registration->registration_student_date_of_birth}}</td>
                <td>{{$registration->registration_date}}</td>
                <td>
                    @if($registration->registration_status == 0)
                        Chưa được xếp lớp
                    @elseif($registration->registration_status == 1)
                        Đã được xếp lớp
                    @elseif($registration->registration_status == 2)
                        Đã học xong
                    @else
                        Không rõ
                    @endif
                </td>
                <td>{{$registration->registration_lesson_left}}</td>
                <td>{{$registration->course_title}}</td>
                <td>{{$registration->registration_student_email}}</td>
                {{-- <td>
                    <a href="{{route('registration.process_delete', ['id' => $registration->registration_id])}}" class="btn btn-danger btn-circle btn-sm">
                    <i class="fas fa-trash"></i>
                  </a> --}}
                    {{-- <a href="{{route('registration.process_delete', ['id' => $registration->registration_id])}}">Xóa</a> --}}
                {{-- </td> --}}
            </tr>
        @endforeach
        </table>
        {{$array_registration->links()}}
@endsection
    </body>
</html>
