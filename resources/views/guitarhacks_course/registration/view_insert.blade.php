<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Đăng ký</title>
</head>
<body>
@extends('layout.main')
@section('content')
    <form action="{{route('registration.process_insert')}}" method="post">
        {{csrf_field()}}
        <h1>Đăng ký khóa học</h1>
        <div class="form-group">
            Họ và tên
            <input type="text" name="registration_student_full_name" class="form-control">
        </div>

        <div class="form-group">
            Số điện thoại
            <input type="text" name="registration_student_phone_number" class="form-control">
        </div>

        <div class="form-group">
            Email
            <input type="email" name="registration_student_email" class="form-control">
        </div>
           
        <div class="row">
            <div class="col">
                Khóa học
                <select name="registration_course_id" class="form-control">
                    @foreach($array_course as $course)
                        <option value="{{$course->course_id}}">{{$course->course_title}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col">
                Ngày sinh
                <input type="date" name="registration_student_date_of_birth" class="form-control">
            </div>
        </div>       
        <br>
        <button type="submit" class="btn btn-primary">Đăng ký</button>      
    </form>
@endsection
</body>
</html>
