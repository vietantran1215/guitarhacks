<form action="{{route('bill.process_insert')}}" method="post">
	 {{csrf_field()}}
	<table>
		<tr>
			<td>Tên khách hàng</td>
			<td>
				<input type="text" name="bill_customer_name">
			</td>
		</tr>
		<tr>
			<td>SĐT khách hàng</td>
			<td>
				<input type="text" name="bill_customer_phone_number">
			</td>
		</tr>
		<tr>
			<td>Tình trạng</td>
			<td>
				<select name="bill_status">
					<option value="1">Có thể order</option>
					<option value="2">Không thể order</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Tên sản phẩm</td>
			<td>
				<select name="bill_product_id">
					@foreach ($array_product as $product)
						<option value="{{$product->product_id}}">
							{{$product->product_name}}
						</option>
					@endforeach
					</select>
			</td>
		</tr>
	</table>
	<button>Thêm</button>
</form>