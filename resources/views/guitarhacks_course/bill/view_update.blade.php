<form action="{{route('bill.process_update')}}" method="post">
	 {{csrf_field()}}
	<table>
		<tr>
			<input type="hidden" name="bill_id" value="{{$bill->bill_id}}">
		</tr>
		<tr>
			<td>Tên khách hàng</td>
			<td>
				<input type="text" name="bill_customer_name" value="{{$bill->bill_customer_name}}">
			</td>
		</tr>
		<tr>
			<td>SĐT khách hàng</td>
			<td>
				<input type="text" name="bill_customer_phone_number" value="{{$bill->bill_customer_phone_number}}">
			</td>
		</tr>
		<tr>
			<td>Tình trạng</td>
			<td>
				<select name="bill_status">
					<option value="1" 
						{{($bill->bill_status == 1)?"selected":""}}
					>Đã thanh toán</option>
					<option value="2" 
						{{($bill->bill_status == 2)?"selected":""}}
					>Chưa thanh toán</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Tên sản phẩm</td>
			<td>
				<select name="bill_product_id">
					@foreach ($array_product as $product)
					<option value="{{$product->product_id}}" >
						{{$product->product_name}}
					</option>
					@endforeach
				</select>
			</td>
		</tr>
	</table>
	<button>Lưu</button>
</form>