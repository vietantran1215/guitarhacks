<!DOCTYPE html>
<html>
<head>
	<title>Hóa đơn</title>
</head>
<body>
@extends('layout.main')
@section('content')
<h1 class="h3 mb-0 text-gray-800">Danh sách hóa đơn</h1>
	<a href="{{route('bill.view_insert')}}" class="btn btn-info btn-circle btn-sm">
		<i class="fas fa-plus"></i>
	</a>
	
	<table class="table table-hover">
		<tr class="tr_text">
			<td>Mã hóa đơn</td>
			<td>Tên học viên</td>
			<td>Số điện thoại</td>
			<td>Tình trạng hóa đơn</td>
			<td>Tên sản phẩm</td>
			<td></td>
			<td></td>
		</tr>
		@foreach($array_bill as $bill)
		<tr>
			<td>{{$bill->bill_id}}</td>
			<td>{{$bill->bill_customer_name}}</td>
			<td>{{$bill->bill_customer_phone_number}}</td>
			<td>
				@if( $bill->bill_status == 1 )
					Đã thanh toán
				@elseif ( $bill->bill_status == 2)
					Chưa thanh toán
				@else
					không rõ
				@endif
			</td>
			<td>{{$bill->product_name}}</td>	
			<td>
				<a href="{{route('bill.view_update',[ 'id'=>$bill->bill_id ])}}" class="btn btn-info btn-circle btn-sm">
					<i class="fas fa-pencil-alt"></i>
				</a>
				
			</td>
			<td>
				<a href="{{route('bill.process_delete',[ 'id'=>$bill->bill_id ])}}" class="btn btn-danger btn-circle btn-sm">
					<i class="fas fa-trash"></i>
				</a>
			</td>
		</tr>
		@endforeach 
	</table>
@endsection
</body>
</html>