<!DOCTYPE html>
<html>
<head>
    <title>Lịch học</title>
</head>
<body >
<center>
<h1>Thông tin khóa học</h1>
<h4 style="color: red">
    Học phí: {{$gclass->course_price}} VNĐ
</h4>
<table border="1px">
    <tr class="tr_text">
        <td>Lớp</td>
        <td>Ngày khai giảng</td>
        <td>Ngày dự kiến kết thúc</td>
        <td>Tình trạng lớp học</td>
        <td>Khóa</td>
        <td>Số buổi còn lại</td>
        <td>Thời gian</td>
    </tr>
    <tr>
        <td>{{$gclass->gclass_name}}</td>
        <td>{{$gclass->gclass_start_date}}</td>
        <td>{{$gclass->gclass_end_date}}</td>
        <td>
            @if($gclass->gclass_status == 0)
                Chưa khai giảng
            @elseif($gclass->gclass_status == 1)
                Đã khai giảng - Đang chạy
            @elseif($gclass->gclass_status == 2)
                Đã kết thúc
            @elseif($gclass->gclass_status == 3)
                Ngày khai giảng
            @endif
        </td>
        <td>{{$gclass->course_title}}</td>
        <td>{{$gclass->gclass_lesson_left}}</td>
        <td>
            @switch($gclass->timetable_day)
                @case(8)
                    Chủ nhật
                    @break
                @case(2)
                    Thứ hai
                    @break
                @case(3)
                    Thứ ba
                    @break
                @case(4)
                    Thứ tư
                    @break
                @case(5)
                    Thứ năm
                    @break
                @case(6)
                    Thứ sáu
                    @break
                @case(7)
                    Thứ bảy
                    @break
                @default
                    Không rõ
                    @break
            @endswitch
            {{$gclass->timetable_time}}
        </td>
    </tr>
</table>
<h3>Chúc các bạn học vui vẻ !!!</h3>
</center>
</body>
</html>