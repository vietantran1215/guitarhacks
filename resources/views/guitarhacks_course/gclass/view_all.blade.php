<!DOCTYPE html>
<html>
<head>
  <title>Lớp học</title>
  
</head>
<body>
@extends('layout.main')
@section('content')
<h1 class="h3 mb-0 text-gray-800">Danh sách lớp học</h1>

<a href="{{route('gclass.view_insert')}}" class="btn btn-info btn-circle btn-sm">
    <i class="fas fa-plus"></i>
</a>
            <table class="table table-hover">
            <tr class="tr_text">
                <td>Lớp</td>
                <td>Ngày khai giảng</td>
                <td>Ngày dự kiến kết thúc</td>
                <td>Tình trạng lớp học</td>
                <td>Khóa</td>
                <td>Học phí</td>
                <td>Số buổi còn lại</td>
                <td>Thời gian</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        @foreach($array_gclass as $gclass)
            <tr>
                <td>{{$gclass->gclass_name}}</td>
                <td>{{$gclass->gclass_start_date}}</td>
                <td>{{$gclass->gclass_end_date}}</td>
                <td>
                    @if($gclass->gclass_status == 0)
                        Chưa khai giảng
                    @elseif($gclass->gclass_status == 1)
                        Đã khai giảng - Đang chạy
                    @elseif($gclass->gclass_status == 2)
                        Đã kết thúc
                    @elseif($gclass->gclass_status == 3)
                        Ngày khai giảng
                    @endif
                </td>
                <td>{{$gclass->course_title}}</td>
                <td>{{$gclass->course_price}} (VNĐ)</td>
                <td>{{$gclass->gclass_lesson_left}}</td>
                <td>
                    @switch($gclass->timetable_day)
                        @case(8)
                            Chủ nhật
                            @break
                        @case(2)
                            Thứ hai
                            @break
                        @case(3)
                            Thứ ba
                            @break
                        @case(4)
                            Thứ tư
                            @break
                        @case(5)
                            Thứ năm
                            @break
                        @case(6)
                            Thứ sáu
                            @break
                        @case(7)
                            Thứ bảy
                            @break
                        @default
                            Không rõ
                            @break
                    @endswitch
                    - {{$gclass->timetable_time}}
                </td>
                <td>
                    <a href="{{route('student.view_all_to_gclass', ['id' => $gclass->gclass_id])}}" class="btn btn-info btn-circle btn-sm">
                        <i class="fas fa-info-circle"></i>
                    </a>
                </td>
                <td>
                    <a href="{{ route('gclass.view',['id'=> $gclass->gclass_id]) }}" class="btn btn-info btn-circle btn-sm">
                        <i class="far fa-calendar-alt"></i>
                    </a> 
                </td>
                <td>
                    <a href="{{route('gclass.view_update', ['id' => $gclass->gclass_id])}}" class="btn btn-info btn-circle btn-sm">
                        <i class="fas fa-pencil-alt"></i>
                    </a> 
                </td>
                <td>
                    <a href="{{route('gclass.process_delete', ['id' => $gclass->gclass_id])}}" class="btn btn-danger btn-circle btn-sm">
                    <i class="fas fa-trash"></i>
                  </a>
                  </a>
                </td>
            </tr>
        @endforeach
        </table>
{{
    $array_gclass->links()
}}
        <!-- jquery -->
        <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
        <!-- update all class status -->
        <script src="{{asset('js/gclass_update_all.js')}}"></script>
        
@endsection
</body>
</html>