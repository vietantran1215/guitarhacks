<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Thêm lớp</title>
    </head>
    <body>
        <form action="{{route('gclass.process_insert')}}" method="post">
            {{csrf_field()}}
            <table>
                <tr>
                    <td>
                        <label for="gclass_name">Tên lớp</label>
                    </td>
                    <td>
                        <input type="text" name="gclass_name" id="gclass_name">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="gclass_start_date">Ngày khai giảng</label>
                    </td>
                    <td>
                        <input type="date" name="gclass_start_date" id="gclass_start_date">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="gclass_end_date">Ngày dự kiến kết thúc</label>
                    </td>
                    <td>
                        <input type="date" name="gclass_end_date" id="gclass_end_date">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="gclass_course_id">Khóa học</label>
                    </td>
                    <td>
                        <select name="gclass_course_id">
                            <option value="{{$course->course_id}}">{{$course->course_title}}</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="gclass_timetable_id">Lịch học</label>
                    </td>
                    <td>
                        <select name="gclass_timetable_id">
                            @foreach($array_timetable as $timetable)
                                <option value="{{$timetable->timetable_id}}">
                                    @switch($timetable->timetable_day)
                                        @case(8)
                                            Chủ nhật
                                            @break
                                        @case(2)
                                            Thứ hai
                                            @break
                                        @case(3)
                                            Thứ ba
                                            @break
                                        @case(4)
                                            Thứ tư
                                            @break
                                        @case(5)
                                            Thứ năm
                                            @break
                                        @case(6)
                                            Thứ sáu
                                            @break
                                        @case(7)
                                            Thứ bảy
                                            @break
                                        @default
                                            Không rõ
                                            @break
                                    @endswitch
                                    - {{$timetable->timetable_time}}
                                </option>
                            @endforeach
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <button>Thêm</button>
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>
