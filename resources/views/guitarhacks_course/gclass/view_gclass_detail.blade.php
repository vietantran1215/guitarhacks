<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Lớp học - Chi tiết</title>
    </head>
    <body>
        @extends('layout.main')
        @section('content')

        <h1 class="h3 mb-0 text-gray-800">Lớp {{$gclass->gclass_name}}</h1>
        <br>
    <div class="row">
        <div class="col-xl-3 col-md-6 mb-4">
          <div class="card border-left-success shadow h-100 py-2">
            <div class="card-body">
              <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                  <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Học phí</div>
                  <div class="h5 mb-0 font-weight-bold text-gray-800">{{$gclass->course_price}}</div>
                </div>
                <div class="col-auto">
                  <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Ngày bắt đầu</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">{{$gclass->gclass_start_date}}</div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        
        <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Ngày kết thúc</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">{{$gclass->gclass_end_date}}</div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Lịch học</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">
                        @switch($gclass->timetable_day)
                        @case(8)
                            Chủ nhật
                            @break
                        @case(2)
                            Thứ hai
                            @break
                        @case(3)
                            Thứ ba
                            @break
                        @case(4)
                            Thứ tư
                            @break
                        @case(5)
                            Thứ năm
                            @break
                        @case(6)
                            Thứ sáu
                            @break
                        @case(7)
                            Thứ bảy
                            @break
                        @default
                            Không rõ
                            @break
                    @endswitch
                        - {{$gclass->timetable_time}}
                      </div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-comments fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
    </div>
        {{-- <table class="timeline">
            <tr>
                <td >
                    <p class="text">Học phí </p>{{$gclass->course_price}}
                    
                </td>
                <td>
                    <p class="text">ngày bắt đầu</p>
                    {{$gclass->gclass_start_date}}
                </td>
                <td>
                    <p class="text">ngày kết thúc</p>
                    {{$gclass->gclass_end_date}}
                </div>
            </tr>
            
        </table> --}}
        <a href="{{ route('button',['gclass_id' => $gclass->gclass_id]) }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-hourglass-start"></i></i> Start</a>
        {{-- <a href="{{ route('button',['gclass_id' => $gclass->gclass_id]) }}" class="btn btn-primary">Start</a> --}}
        <br>

        <form action="{{ route('send_mail')}}">
             {{csrf_field()}}
            <table  class="table table-hover">
            <tr class="tr_text">
                <td>Họ và tên</td>
                <td>Số điện thoại</td>
                <td>Ngày sinh</td>
                <td>Số buổi còn lại</td>
                <td>Email</td>
                <td>Lớp</td>
                <td></td>
                
            </tr>
            @foreach($array_student as $student)
            <tr>
                <td>{{$student->registration_student_full_name}}</td>
                <td>{{$student->registration_student_phone_number}}</td>
                <td>{{$student->registration_student_date_of_birth}}</td>
                <td>{{$student->gclass_lesson_left}}</td>
                <td>{{$student->email}}</td>
                <td>{{$student->gclass_name}}</td>
                <td>
                    <a href="{{route('student.process_unset_gclass', [
                    'student_id' => $student->student_id,
                    'gclass_id'  => $gclass->gclass_id,
                    'student_email'=>$student->email
                    ])}}" class="btn btn-danger btn-circle btn-sm">
                    <i class="fas fa-trash"></i>
                  </a>
                    {{-- <a href="{{route('student.process_unset_gclass', [
                    'student_id' => $student->student_id,
                    'gclass_id'  => $gclass->gclass_id,
                    'student_email'=>$student->email
                    ])}}">
                        Xóa
                    </a> --}}
                </td>
            </tr>
            @endforeach
        </table>
        {{$array_student->links()}}
        </form>
        
<hr >
<br>
        <h1 class="h3 mb-0 text-gray-800">Thêm học viên</h1>
        <table  class="table table-hover">
            <tr style="font-weight: 600; color: #5a5c69">
                <td>Họ và tên</td>
                <td>Số điện thoại</td>
                <td>Ngày sinh</td>
                <td>Email</td>
                <td>Số buổi còn lại</td>
                <td>Ngày đăng ký</td>
                <td>Tình trạng</td>
                <td></td>
            </tr>
        @foreach($array_unset_student as $student)
            <tr>
                <td>{{$student->registration_student_full_name}}</td>
                <td>{{$student->registration_student_phone_number}}</td>
                <td>{{$student->registration_student_date_of_birth}}</td>
                <td>{{$student->email}}</td>
                <td>{{$student->registration_lesson_left}}</td>
                <td>{{$student->registration_date}}</td>
                <td>
                    @switch($student->registration_status)
                        @case (0)
                            Chưa được xếp lớp
                            @break
                        @case (1)
                            Đã được xếp lớp
                            @break
                        @case (2)
                            Hoàn thành khóa học
                            @break
                        @default
                            Không rõ
                            @break
                    @endswitch
                </td>
                <td>
                    @if($student->registration_status == 0)
                        <a href="{{route('student.process_set_gclass', [
                        'gclass_id' => $gclass->gclass_id,
                        'student_id' => $student->student_id,
                        'student_email'=>$student->email,
                        
                        ])}}">
                            Thêm
                        </a>
                    @endif
                </td>
            </tr>
        @endforeach
        </table>
        @endsection
    </body>
</html>
