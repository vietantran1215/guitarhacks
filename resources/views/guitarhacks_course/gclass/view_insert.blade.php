<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Lớp học - Thêm</title>
</head>
<body>
@extends('layout.main')
@section('content')      
      <h1>Thêm lớp</h1>
      <form  action="{{route('gclass.process_insert')}}" method="post">
         {{csrf_field()}}
          <div class="form-group">
            Tên lớp
            <input type="text" class="form-control" id="gclass_name" placeholder="Điền tên..." name="gclass_name">
          </div>
          
          <div class="form-row">
            <div class="form-group col-md-6">
              Ngày khai giảng
              <input type="date" class="form-control" name="gclass_start_date" id="gclass_start_date">
            </div>
            <div class="form-group col-md-6">
              Ngày kết thúc
              <input type="date" class="form-control" name="gclass_end_date" id="gclass_end_date" >
            </div>
          </div>
          
          <div class="form-row">
            <div class="form-group col-md-6">
              Khóa học
              <select name="gclass_course_id" class="form-control">
                @foreach($array_course as $course)
                    <option value="{{$course->course_id}}" >
                      {{$course->course_title}}
                    </option>
                @endforeach
              </select>
            </div>
            <div class="form-group col-md-6">
              Lịch học
              <select name="gclass_timetable_id" class="form-control">
                @foreach($array_timetable as $timetable)
                    <option value="{{$timetable->timetable_id}}" >
                        @switch($timetable->timetable_day)
                            @case(8)
                                Chủ nhật
                                @break
                            @case(2)
                                Thứ hai
                                @break
                            @case(3)
                                Thứ ba
                                @break
                            @case(4)
                                Thứ tư
                                @break
                            @case(5)
                                Thứ năm
                                @break
                            @case(6)
                                Thứ sáu
                                @break
                            @case(7)
                                Thứ bảy
                                @break
                            @default
                                Không rõ
                                @break
                        @endswitch
                        - {{$timetable->timetable_time}}
                    </option>
                @endforeach
              </select>
            </div>
          </div>
          <button type="submit" class="btn btn-primary">Thêm lớp</button>
      </form>
@endsection
</body>
</html>
