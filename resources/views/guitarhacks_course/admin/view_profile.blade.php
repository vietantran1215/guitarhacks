<!DOCTYPE html>
<html>
<head>
  <title>Admin - Trang thông tin</title>
</head>
<body>
@extends('layout.main')
@section('content') 
 <center>
   <div class="container1">
    <h1><b>Profile Admin</b></h1>
    <h2>HTML + CSS ONLY</h2>
    <div class="service-details">
      <img src="{{asset("admin/img/admin_img/$admin->img")}}" alt="realm">
      <div class="service-hover-text">
        <h3>{{$admin->admin_full_name}}</h3>
        <h4>Admin</h4>
        <p>
          Email: {{$admin->admin_email}}
          <br>
          Password: {{$admin->admin_password}}
        </p>
      </div>
      <div class="service-white service-text">
        <p>{{$admin->admin_full_name}}</p>
      </div>
    </div>
  </div>
 </center>
@endsection
</body>
</html>