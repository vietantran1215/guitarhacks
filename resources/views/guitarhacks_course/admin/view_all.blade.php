<!DOCTYPE html>
<html>
<head>
  <title>Admin</title>
</head>
<body>
@extends('layout.main')
@section('content')
  <h1 class="h3 mb-0 text-gray-800">Admin</h1>
  <table class="table table-hover">
      <tr class="tr_text">
        <td>Họ và tên</td>
        <td>Email</td>
        <td>Ảnh</td>
      </tr>
      @foreach($array_admin as $admin)
      <tr>
        <td>{{$admin->admin_full_name}}</td>
        <td>{{$admin->admin_email}}</td>
        <td>
          <img src="{{asset("admin/img/admin_img/$admin->img")}}" width="200"height="200">
        </td>
      </tr>
      @endforeach 
  </table>
    {{
      $array_admin->links()
    }} 
@endsection 
</body>
</html>