<!DOCTYPE html>
<html>
<head>
  <title>Admin - Sửa thông tin</title>
</head>
<body>
@extends('layout.main')
@section('content')
{{-- <h1 class="h3 mb-0 text-gray-800">Chỉnh sửa trang cá nhân</h1> --}}
<div class="container2">

  <h2>Sửa thông tin cá nhân <small>Inputs</small></h2>

<form action="{{ route('admin.process_upload',['id'=>$admin->admin_id]) }}" enctype="multipart/form-data" method="POST">
  {{ csrf_field() }}
    <input type="hidden" name="admin_id" value="{{$admin->admin_id}}">
    <div class="group">
      <input input type="text" name="admin_full_name" value="{{$admin->admin_full_name}}" required>
      <span class="highlight"></span>
      <span class="bar"></span>
      <label>Name</label>
    </div>

    <div class="group">
      <input type="email" name="admin_email" value="{{$admin->admin_email}}" required>
      <span class="highlight"></span>
      <span class="bar"></span>
      <label>Email</label>
    </div>

     <div class="group">
      <input type="text" name="admin_password" value="{{$admin->admin_password}}" required>
      <span class="highlight"></span>
      <span class="bar"></span>
      <label>Password</label>
    </div>
    <input type="file" name="img" accept="image/png, image/jpeg">
    <br>
   <input type="submit" name="" value="SAVE">
</form>
</div>
@endsection
</body>
</html>
