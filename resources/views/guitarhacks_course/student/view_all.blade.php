<!DOCTYPE html>
<html>
<head>
  <title></title>
 {{--  <link rel="stylesheet" type="text/css" href="{{ asset('admin/vendor/datatables/dataTables.bootstrap4.min.css') }}"> --}}
</head>
<body>
@extends('layout.main')
@section('title')
Học viên
@endsection
@section('content')
<h1 class="h3 mb-0 text-gray-800">Danh sách học viên</h1>

    <table >
        <tr>
        <thead>
            <td>Họ và tên</td>
            <td>Số điện thoại</td>
            <td>Ngày sinh</td>
            <td>Email</td>
            <td>Số buổi còn lại</td>
            <td>Khóa</td>
            <td></td>
            
        </thead>
        </tr>
        <tbody>
            <tr>
                @foreach($array_student as $student)
            <tr>
                <td>{{$student->registration_student_full_name}}</td>
                <td>{{$student->registration_student_phone_number}}</td>
                <td>{{$student->registration_student_date_of_birth}}</td>
                <td>{{$student->registration_student_email}}</td>
                <td>{{$student->registration_lesson_left}}</td>
                <td>{{$student->course_title}}</td>
                <td>
                    <a href="{{ route('student.process_delete',['$student_id'=>$student->student_id]) }}" class="btn btn-danger btn-circle btn-sm">
                    <i class="fas fa-trash"></i>
                  </a>
                    {{-- <a href="{{ route('student.process_delete',['$student_id'=>$student->student_id]) }}">Xóa</a> --}}
                </td>
            </tr>
        @endforeach
            </tr>
        </tbody>
        
    </table>
@endsection
{{-- <script src="{{ asset('admin/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('admin/js/demo/datatables-demo.js') }}"></script> --}}
</body>
</html>