<!DOCTYPE html>
<html>
<head>
	<title>
		Bài viết
	</title>
</head>
<body>
@extends('layout.main')

@section('content')
	<h1 class="h3 mb-0 text-gray-800">Bài Đăng</h1>
	<a href="{{ route('blog.view_insert') }}" class="btn btn-info btn-circle btn-sm">
		<i class="fas fa-plus"></i>
	</a>
	<table class="table table-hover">
		<tr class="tr_text">
			<td>Tiêu đề bài đăng</td>
			<td>Ngày giờ đăng</td>
			<td>Số lượt xem</td>
			{{-- <td>Nội dung</td> --}}
			<td>Trạng thái</td>
			<td>Người đăng</td>
			<td></td>
			<td></td>

		</tr>
		@foreach($array_blog as $blog)
		<tr>

			<td>
				<p  class="module">{{$blog->blog_title}}</p>
			</td>
			<td>{{$blog->blog_published_datetime}}</td>
			<td>{{$blog->blog_views}}</td>
			{{-- <td>
				<p class="module">{!!$blog->blog_content!!}</p>
			</td> --}}
			<td>
				@if($blog->blog_status == 0)
                    Chưa duyệt
                @elseif($blog->blog_status == 1)
                    Đã duyệt
                @else
                    Không rõ
                @endif
			</td>
			<td>{{$blog->admin_full_name}}</td>
			<td>
				<a href="{{ route('blog.view_update',['id'=>$blog->blog_id]) }}" class="btn btn-info btn-circle btn-sm">
					<i class="fas fa-pencil-alt"></i>
				</a>
			</td>
			<td>
				<a href="{{ route('blog.process_delete',['id'=>$blog->blog_id]) }}" class="btn btn-danger btn-circle btn-sm">
					<i class="fas fa-trash"></i>
				</a>
			</td>

		</tr>
		@endforeach
	</table>
	{{$array_blog->links()}}
@endsection
</body>
</html>
