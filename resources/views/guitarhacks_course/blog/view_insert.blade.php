
<!DOCTYPE html>
<html>
<head>
	<title>Bài viết - Thêm</title>
	<script type="text/javascript" src="{{ asset('ckeditor/ckeditor.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ asset('admin/css/blog_insert.css') }}">
</head>
<body>
@extends('layout.main')
@section('content')
	<div class="section-form">
	  <form action="{{ route('blog.process_insert') }}" method="post">
	  {{csrf_field()}}
	    <div class="intro"> 
	      <h1 class="h3 mb-0 text-gray-800">Thêm bài viết</h1>
	      {{-- <h2>Get your project started today</h2>  --}}
	    </div>
	    <div>Tiêu đề</div>
	    <textarea  name=" blog_title" tabindex="1" rows="1" placeholder="Tiêu đề"></textarea>
	    <div>Nội dung</div>
	    <textarea name=" blog_content" class="ckeditor" id="editor" tabindex="3" ></textarea>
	    <div>Thời gian</div>
	    <input type="datetime" name="blog_published_datetime" value="{{$dt->toDateTimeString()}}"tabindex="2"/>
	    <br>
	    <input type="hidden" name="admin_id" value="{{Session::get('admin_id')}}">
	    <button>Thêm bài viết</button>

	  </form>
</div>

@endsection
</body>
</html>