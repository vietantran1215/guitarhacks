<!DOCTYPE html>
<html>
<head>
	<title>Bài viết - Sửa</title>
	<script type="text/javascript" src="{{ asset('ckeditor/ckeditor.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ asset('admin/css/blog_insert.css') }}">
</head>
<body>
@extends('layout.main')
@section('content')
	<div class="section-form">
		<form action="{{ route('blog.process_update') }}" method="post">
			@csrf
			<h1>Sửa bài viết</h1>
			<input type="hidden" name="blog_id" value="{{$blog->blog_id}}">

			<div>Tiêu đề</div>
		    <textarea  name=" blog_title" tabindex="1" rows="1" placeholder="Tiêu đề">
		    	{{$blog->blog_title}}
		    </textarea>

			<div>Nội dung</div>
	    	<textarea name=" blog_content" class="ckeditor" tabindex="3" >
	    		{{$blog->blog_content}}
	    	</textarea>

			<div>Thời gian</div>
	    	<input type="datetime" name="blog_published_datetime" value="{{$dt->toDateTimeString()}}"tabindex="2"/>
 
 			<select name="blog_status">
 				<option value="0" {{($blog->blog_status == 0)? "selected": ""}}>Ẩn</option>
 				<option value="1" {{($blog->blog_status == 1)? "selected": ""}}>Công khai</option>
 			</select>
 			<br>
			<input type="hidden" name="admin_id" value="{{Session::get('admin_id')}}">
			<br>
			<button>Sửa</button>
		</form>
	</div>
@endsection
</body>
</html>