<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
@extends('layout.main')
@section('content')
<h1 class="h3 mb-0 text-gray-800">Sửa sản phẩm</h1>
    <form action="{{route('product.process_update')}}" method="POST">
      {{csrf_field()}}
          <input type="hidden" name="product_id" value="{{ $product->product_id}}">
      <div class="form-group">
          Tên sản phẩm
          <input  type="text" class="form-control" name="product_name" value="{{$product->product_name}}" />
      </div>
      
      <div>
          Giá sản phẩm
          <input type="text" class="form-control" name="product_price" value="{{$product->product_price}}">
      </div>

      <div class="form-group">
          Mô tả
          <textarea class="form-control ckeditor" name="product_description" id="editor" rows="3">{{$product->product_description}}</textarea>
      </div>

      <div >
        <button class="btn btn-primary btn-lg">Lưu lại</button>
      </div>
    </form>
@endsection
</body>
</html>