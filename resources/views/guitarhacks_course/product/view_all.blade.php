<!DOCTYPE html>
<html>
<head>
  <title>Sản phẩm</title>
  
</head>
<body>
@extends('layout.main')
@section('content')
<h1 class="h3 mb-0 text-gray-800">Danh sách sản phẩm</h1>

<a href="{{route('product.view_insert')}}" class="btn btn-info btn-circle btn-sm">
    <i class="fas fa-plus"></i>
</a>
        <table class="table table-hover">
            <tr class="tr_text">
                <td>Gói sản phẩm</td>
                <td>Mô tả</td>
                <td>Giá</td>
                <td></td>
                <td></td>
            </tr>
        @foreach($array_product as $product)
            <tr>
                <td>{{$product->product_name}}</td>
                <td>{!!$product->product_description!!}</td>
                <td>{{number_format($product->product_price). " VNĐ"}}</td>
                <td>
                    <a href="{{Route('product.view_update',['id' => $product->product_id])}}" class="btn btn-info btn-circle btn-sm">
                    <i class="fas fa-pencil-alt"></i>
                    </a>
                </td>
                <td>
                    <a href="{{Route('product.process_delete',['id' => $product->product_id])}}" class="btn btn-danger btn-circle btn-sm">
                    <i class="fas fa-trash"></i>
                  </a>
                </td>
            </tr>
        @endforeach
        </table>
        {{$array_product->links()}}
@endsection
</body>
</html>