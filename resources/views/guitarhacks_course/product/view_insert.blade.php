<!DOCTYPE html>
<html>
<head>
 
</head>
<body>

@extends('layout.main')    
@section('content')
  <h1 class="h3 mb-0 text-gray-800">Thêm sản phẩm</h1>
      <form class="form" id="form1" action="{{route('product.process_insert')}}" method="post" enctype="multipart/form-data">
         {{csrf_field()}}
        <div class="form-group">
          Tên sản phẩm
          <input  class="form-control" type="text" id="product_name" name="product_name" >
        </div>
          
        <div class="form-group">
          Giá sản phẩm
          <input class="form-control" type="text" id="product_price" name="product_price" >
        </div>

        <div class="form-group">
          Mô tả
          <textarea class="form-control ckeditor" id="editor" name="product_description" rows="3"></textarea>
        </div>
        <div>
          <button class="btn btn-primary btn-lg">Thêm sản phẩm</button>
        </div>
      </form>
@endsection
  <script type="text/javascript" src="{{ asset('ckeditor/ckeditor.js') }}"></script>
  <script type="text/javascript">
    CKEDITOR.replace('editor',{
      filebrowserBrowseUrl: '{{ asset('ckfinder/ckfinder.html') }}',
          filebrowserImageBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Images') }}',
          filebrowserFlashBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Flash') }}',
          filebrowserUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
          filebrowserImageUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
          filebrowserFlashUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
      })
  </script>
</body>
</html>