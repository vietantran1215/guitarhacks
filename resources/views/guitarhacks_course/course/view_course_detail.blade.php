<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Khóa học - Chi tiết</title>
    </head>
    <body>
        @extends('layout.main')
        @section('content')
    <!-- <a id="realtime_update" href="{{route('gclass.update_all')}}">update</a> -->
        <h2 class="h3 mb-0 text-gray-800">Các lớp học khóa {{$course->course_title}}</h2>
        <a href="{{route('gclass.view_insert_to_course', ['id' => $course->course_id])}}" class="btn btn-info btn-circle btn-sm">
            <i class="fas fa-plus"></i>
        </a>
        <div>
            <h3 style="color: #1cc88a">Mô tả</h3>
            {!!$course->course_description!!}
        </div>
        <br>
        <h3 style="color: #1cc88a">Danh sách lớp</h3>
        <table class="table table-hover">
            <tr class="tr_text">
                <td>Lớp</td>
                <td>Ngày khai giảng</td>
                <td>Ngày dự kiến kết thúc</td>
                <td>Tình trạng lớp học</td>
                <td>Lớp</td>
                <td>Số buổi còn lại</td>
                <td>Thời gian</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        @foreach($array_gclass as $gclass)
            <tr>
                <td>{{$gclass->gclass_name}}</td>
                <td>{{$gclass->gclass_start_date}}</td>
                <td>{{$gclass->gclass_end_date}}</td>
                <td>
                    @if($gclass->gclass_status == 0)
                        Chưa khai giảng
                    @elseif($gclass->gclass_status == 1)
                        Đã khai giảng - Đang chạy
                    @elseif($gclass->gclass_status == 2)
                        Đã kết thúc
                    @elseif($gclass->gclass_status == 3)
                        Ngày khai giảng
                    @endif
                </td>
                <td>{{$gclass->course_title}}</td>
                <td>{{$gclass->gclass_lesson_left}}</td>
                <td>
                    @switch($gclass->timetable_day)
                        @case(8)
                            Chủ nhật
                            @break
                        @case(2)
                            Thứ hai
                            @break
                        @case(3)
                            Thứ ba
                            @break
                        @case(4)
                            Thứ tư
                            @break
                        @case(5)
                            Thứ năm
                            @break
                        @case(6)
                            Thứ sáu
                            @break
                        @case(7)
                            Thứ bảy
                            @break
                        @default
                            Không rõ
                            @break
                    @endswitch
                    - {{$gclass->timetable_time}}
                </td>
                <td>
                    <a href="{{route('student.view_all_to_gclass', ['id' => $gclass->gclass_id])}}" class="btn btn-info btn-circle btn-sm">
                    <i class="fas fa-info-circle"></i>
                    </a>
                   {{--  <a href="{{route('student.view_all_to_gclass', ['id' => $gclass->gclass_id])}}">Chi tiết lớp học</a> --}}
                </td>
                <td>
                    <a href="{{route('gclass.view_update', ['id' => $gclass->gclass_id])}}" class="btn btn-info btn-circle btn-sm">
                    <i class="fas fa-pencil-alt"></i>
                    </a>
                    {{-- <a href="{{route('gclass.view_update', ['id' => $gclass->gclass_id])}}">Sửa</a> --}}
                </td>
                <td>
                    <a href="{{route('gclass.process_delete', ['id' => $gclass->gclass_id])}}" class="btn btn-danger btn-circle btn-sm">
                    <i class="fas fa-trash"></i>
                  </a>
                    {{-- <a href="{{route('gclass.process_delete', ['id' => $gclass->gclass_id])}}">Xóa</a> --}}
                </td>
            </tr>
        @endforeach
        </table>
        @endsection
        <!-- jquery -->
        <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
        <!-- update all class status -->
        <script src="{{asset('js/gclass_update_all.js')}}"></script>
    </body>
</html>
