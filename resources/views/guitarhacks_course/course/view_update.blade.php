<!DOCTYPE html>
<html>
<head>
  <title>Khóa học - Sửa</title>
</head>
@extends('layout.main')
@section('content')
<form action="{{route('course.process_update')}}" enctype="multipart/form-data" method="POST">
{{csrf_field()}}
    <h1 class="h3 mb-0 text-gray-800">Sửa khóa học</h1>
          <input type="hidden" name="course_id" value="{{$course->course_id}}">
          <div class="form-group">
              Tên khóa học
              <input type="text" name="course_title" value="{{$course->course_title}}" class="form-control" >
          </div>

          <div class="form-group">
              Giá
              <input type="text" name="course_price" value="{{$course->course_price}}" class="form-control">
          </div>

          <div class="form-group">
              Số buổi
              <input name="course_duration" value="{{$course->course_duration}}" class="form-control">
          </div>

          <div class="form-group">
              Mô tả
              <textarea name="course_description" class="ckeditor" id="editor">
                {{$course->course_description}}
              </textarea>
          </div>
        
          <div>
              <button class="btn btn-primary btn-lg">Lưu lại</button>
          </div>
@endsection
</form>
</body>
</html>