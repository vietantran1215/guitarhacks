<!DOCTYPE html>
<html>
<head>
  <title>Khóa học - Thêm</title>
</head>
<body>
@extends('layout.main')
@section('content')
<form action="{{route('course.process_insert')}}" enctype="multipart/form-data" method="POST">
{{csrf_field()}}
    <h1 class="h3 mb-0 text-gray-800">Thêm khóa học</h1>
        <div class="form-group">
            Tên khóa
            <input class="form-control" type="text" name="course_title"/>
        </div>

        <div class="form-group">
            Giá
            <input class="form-control" type="text" name="course_price">
        </div>

        <div class="form-group">
            Số buổi
            <input class="form-control" type="text" name="course_duration">
        </div>

        <div class="form-group">
            Mô tả
            <textarea name="course_description"  class="form-control ckeditor" id="editor"></textarea>
        </div>
        
        <div>
            <button class="btn btn-primary btn-lg">Thêm khóa học</button>
        </div>
</form>
@endsection
</body>
</html