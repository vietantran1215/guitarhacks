<!DOCTYPE html>
<html>
<head>
  <title>Khóa học</title>
</head>
<body>
@extends('layout.main')
@section('content')
<h1 class="h3 mb-0 text-gray-800">Khóa học</h1>
<br>
<a href="{{route('course.view_insert')}}" class="btn btn-info btn-circle btn-sm">
    <i class="fas fa-plus"></i>
</a>
<table class="table table-hover">
    <tr class="tr_text">
        <td>Khóa học</td>
        <td>Số buổi</td>
        <td>Học phí</td>
        <td>Mô tả</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    @foreach($array_course as $course)
    <tr>
        <td>
            <p class="module">{{$course->course_title}}</p>
        </td>
        <td>{{$course->course_duration}}</td>
        <td>{{number_format($course->course_price). " VNĐ"}}</td>
        <td>
            <p class="module">{{$course->course_description}}</p>
        </td>
        <td>
            <a href="{{route('gclass.view_all_to_course', ['id' => $course->course_id])}}" class="btn btn-info btn-circle btn-sm">
                <i class="fas fa-info-circle"></i>
            </a>
    
        </td>
        <td>
            <a href="{{route('course.view_update', ['id' => $course->course_id])}}" class="btn btn-info btn-circle btn-sm">
                <i class="fas fa-pencil-alt"></i>
            </a> 
        </td>
        <td>
            <a href="{{route('course.process_delete', ['id' => $course->course_id])}}" class="btn btn-danger btn-circle btn-sm">
                <i class="fas fa-trash"></i>
            </a>
        </td>
    </tr>
    @endforeach
</table>
    {{$array_course->links()}}
@endsection
</body>
</html>