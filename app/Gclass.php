<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Gclass
{
    private $table = "gclass";
    public $gclass_id;
    public $gclass_name;
    public $gclass_start_date;
    public $gclass_end_date;
    public $gclass_lesson_left;
    public $gclass_status;
    public $gclass_course_id;
    public $gclass_timetable_id;
    public function view_all($search){
        $array_gclass = DB::table($this->table)
                        ->join('course', 'course.course_id', '=', $this->table.'.gclass_course_id')
                        ->join('timetable', 'timetable.timetable_id', '=', $this->table.'.gclass_timetable_id')->where('course_title','like',"%$search%")
                        ->paginate(5);
        return $array_gclass;
    }

    public function view($search)
    {
        $gclass = DB::table($this->table)
                        ->join('course', 'course.course_id', '=', $this->table.'.gclass_course_id')
                        ->join('timetable', 'timetable.timetable_id', '=', $this->table.'.gclass_timetable_id')->where('gclass_id', $this->gclass_id)->get()->first();
        return $gclass;
    }
    public function update_all($today){
        DB::table($this->table)
            ->where('gclass_start_date', '>', $today)
            ->update([
                'gclass_status' => 0
        ]);
        DB::table($this->table)
            ->where([
                ['gclass_start_date', '<', $today],
                ['gclass_end_date', '>', $today]
            ])
            ->update([
                'gclass_status' => 1
        ]);
        DB::table($this->table)
            ->where([
                ['gclass_start_date', '=', $today]
            ])
            ->update([
                'gclass_status' => 3
        ]);
        DB::table($this->table)
            ->where('gclass_end_date', '<', $today)
            ->update([
                'gclass_status' => 2
        ]);
    }
    public function process_insert(){
        DB::table($this->table)
        ->insert([
            'gclass_id'           => $this->gclass_id,
            'gclass_name'         => $this->gclass_name,
            'gclass_start_date'   => $this->gclass_start_date,
            'gclass_end_date'     => $this->gclass_end_date,
            'gclass_lesson_left'  => $this->gclass_lesson_left,
            'gclass_course_id'    => $this->gclass_course_id,
            'gclass_timetable_id' => $this->gclass_timetable_id
        ]);
    }
    public function view_one(){
        $array_gclass = DB::table($this->table)
                            ->where('gclass_id', $this->gclass_id)
                            ->get()->first();
        return $array_gclass;
    }
    public function process_update(){
        DB::table($this->table)
        ->where('gclass_id', $this->gclass_id)
        ->update([
            'gclass_id'          => $this->gclass_id,
            'gclass_name'        => $this->gclass_name,
            'gclass_start_date'  => $this->gclass_start_date,
            'gclass_end_date'    => $this->gclass_end_date,
            'gclass_lesson_left' => $this->gclass_lesson_left,
            'gclass_course_id'   => $this->gclass_course_id
        ]);
    }
    public function process_delete(){
        DB::table($this->table)
        ->where('gclass_id', $this->gclass_id)
        ->delete();
    }
    public function view_all_to_course(){
        $array_gclass = DB::table($this->table)
                            ->where('gclass_course_id', $this->gclass_course_id)
                            ->join('course', 'course.course_id', '=', $this->table.'.gclass_course_id')
                            ->join('timetable', 'timetable.timetable_id', '=', $this->table.'.gclass_timetable_id')
                            ->get();
        return $array_gclass;
    }
    public function button()
    {
       $button = DB::table($this->table)->where('gclass_course_id', $this->gclass_course_id)->decrement('gclass_lesson_left');
       // dd($button);
        
    }
}
