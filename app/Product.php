<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Product extends Model
{
    protected $table = "product";
    protected $primaryKey = "product_id";
    protected $fillable = ['product_name','product_description','product_price'];
    public $product_id;
    public $product_name;
    public $product_description;
    public $product_price;
    public $timestamps = false;

    public function view_all($search){
        $array_product = DB::connection($this->connection)->table($this->table)->where('product_name','like',"%$search%")->paginate(5);
        return $array_product;
    }
    public function process_insert()
    {
       DB::connection($this->connection)->table($this->table)->insert([
        'product_name'          =>$this->product_name,
        'product_description'   =>$this->product_description,
        'product_price'         =>$this->product_price
       ]);
    }
    public function view_update()
    {
        $array_product = DB::connection($this->connection)->table($this->table)->where('product_id', $this->product_id)->get()->first();
        // dd($array_product);
        return $array_product;
    }
    public function process_update()
    {

       DB::connection($this->connection)->table($this->table)->where('product_id',$this->product_id)
      ->update([
         'product_name' => $this->product_name,
         'product_description' => $this->product_description,
         'product_price' =>$this->product_price
      ]);
    }
    public function process_delete()
    {
         DB::connection($this->connection)->table($this->table)->where('product_id',$this->product_id)->delete();
    }
}
