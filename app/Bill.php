<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Bill extends Model
{
    protected $table = "bill";
    protected $connection = "mysql";
    protected $primaryKey = "bill_id";
    protected $fillable = ['bill_customer_name','bill_status','bill_product_id','bill_customer_phone_number'];
    public $bill_id;
    public $bill_customer_name;
    public $bill_customer_phone_number;
    public $bill_status;
    public $bill_product_id;
    public $timestamps= false;
  	
  	public function view_all($search)
  	{
  		$array_bill = DB::connection($this->connection)->table($this->table)->join('product', 'bill.bill_product_id', '=', 'product.product_id')->where('bill_customer_name','like',"%$search%")->paginate(5);
  		return $array_bill;
  	}

  	public function process_insert()
  	{
  		DB::connection($this->connection)->table($this->table)->insert([
  			'bill_customer_name'=>$this->bill_customer_name,
  			'bill_customer_phone_number'=>$this->bill_customer_phone_number,
  			'bill_status'=>$this->bill_status,
  			'bill_product_id'=>$this->bill_product_id
  		]);
  	}

  	public function view_one()
  	{
  		$array_bill = DB::connection($this->connection)->table($this->table)->get()->first();
  		return $array_bill;
  	}
    public function process_update()
    {
      DB::connection($this->connection)->table($this->table)->where('bill_id',$this->bill_id)->update([
        'bill_customer_name'=>$this->bill_customer_name,
        'bill_customer_phone_number'=>$this->bill_customer_phone_number,
        'bill_status'=>$this->bill_status,
        'bill_product_id'=>$this->bill_product_id
      ]);

    }
    public function process_delete()
    {
      DB::connection($this->connection)->table($this->table)->where('bill_id',$this->bill_id)->delete();
    }

}
