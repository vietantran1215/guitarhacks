<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Course extends Model
{
    protected $table = "course";
    protected $primaryKey = "course_id";
    protected $fillable = ['course_title','course_description','course_duration','course_price'];
    public $course_id;
    public $course_title;
    public $course_description;
    public $course_duration;
    public $course_price;
    public $timestamps = false;
    
    public function view_all($search){
        $array_course = DB::connection($this->connection)->table($this->table)->where('course_title','like',"%$search%")->paginate(5);
        return $array_course;
    }
    public function get_class($course_id)
    {
        $array_course = DB::connection($this->connection)->table('gclass')->join('course','course.course_id','=','gclass.gclass_course_id')->where('course_id', $this->course_id)
                                           ->get();
    }
    public function process_insert(){
        DB::connection($this->connection)->table($this->table)
        ->insert([
            'course_title'       => $this->course_title,
            'course_description' => $this->course_description,
            'course_duration'    => $this->course_duration,
            'course_price'       => $this->course_price
        ]);
    }
    public function view_one(){
        $array_course = DB::connection($this->connection)->table($this->table)
                            ->where('course_id', $this->course_id)
                            ->get()->first();
        return $array_course;
    }
    public function view_one_to_gclass($gclass_course_id){
        // Return course to gclass update form
        $array_course = DB::connection($this->connection)->table($this->table)
                            ->where('course_id', $gclass_course_id)
                            ->get()->first();
        return $array_course;
    }
    public function process_update(){
        DB::connection($this->connection)->table($this->table)
        ->where('course_id', $this->course_id)
        ->update([
            'course_title'       => $this->course_title,
            'course_description' => $this->course_description,
            'course_duration'    => $this->course_duration,
            'course_price'       => $this->course_price
        ]);
    }
    public function process_delete(){
        DB::connection($this->connection)->table($this->table)
        ->where('course_id', $this->course_id)
        ->delete();
    }
}
