<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Student
{
    private $table = "student";
    private $connection = "mysql";
    public $student_id;
    public $student_registration_id;
    public $student_gclass_id;

    public function view_all($search){
        $array_student = DB::connection($this->connection)
                            ->table($this->table)
                            ->join('registration', 'registration.registration_id', '=', $this->table.'.student_registration_id')
                            ->join('course','registration.registration_course_id','=','course.course_id')
                            ->where('registration_student_full_name','like',"%$search%")
                            ->paginate(7);
        return $array_student;
    }
    public function view_all_to_gclass(){
        $array_student = DB::connection($this->connection)
                            ->table($this->table)
                            ->join('gclass', 'gclass.gclass_id', '=', $this->table.'.student_gclass_id')
                            ->join('registration', 'registration.registration_id', '=', $this->table.'.student_registration_id')
                            ->where($this->table.'.student_gclass_id', $this->student_gclass_id)
                            ->paginate(3);
        return $array_student;
    }
    public function view_all_unset_to_gclass($course_id){
        $array_unset_student = DB::connection($this->connection)
                                ->table($this->table)
                                ->join('registration', 'registration.registration_id', '=', $this->table.'.student_registration_id')
                                ->where('registration.registration_course_id', '=', $course_id)
                                ->orderBy('registration.registration_status', 'asc')
                                ->paginate(3);
        // print_r($array_unset_student);dd();
        return $array_unset_student;
    }
    public function process_insert(){
        DB::connection($this->connection)
            ->table($this->table)
            ->insert([
                'student_registration_id' => $this->student_registration_id
            ]);
    }
    public function view_one(){
        $student = DB::connection($this->connection)
                    ->table($this->table)
                    ->where('student_id', '=', $this->student_id)
                    ->get()->first();
        return $student;
    }
    public function process_set_gclass(){
        DB::connection($this->connection)
            ->table($this->table)
            ->where('student_id', '=', $this->student_id)
            ->update([
                'student_gclass_id' => $this->student_gclass_id
            ]);
    }
    public function process_unset_gclass(){
        DB::connection($this->connection)
            ->table($this->table)
            ->where('student_id', '=', $this->student_id)
            ->update([
                'student_gclass_id' => NULL
            ]);
    }
    public function process_delete()
    {
        DB::connection($this->connection)->table($this->table)->where('student_id',$this->student_id)->delete();
    }
}

