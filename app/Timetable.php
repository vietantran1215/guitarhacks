<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Timetable
{
    private $table = 'timetable';
    private $connection = "mysql";
    public $timetable_id;
    public $timetable_day;
    public $timetable_time;
    public function view_all(){
        $array_timetable = DB::connection($this->connection)
                            ->table($this->table)
                            ->orderBy('timetable_day')
                            ->paginate(3);
        return $array_timetable;
    }
    public function process_insert(){
        DB::connection($this->connection)->table($this->table)
            ->insert([
                'timetable_day'  => $this->timetable_day,
                'timetable_time' => $this->timetable_time
            ]);
    }
    public function process_delete(){
        DB::connection($this->connection)->table($this->table)
            ->where('timetable_id', $this->timetable_id)
            ->delete();
    }
}
