<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;

class Blog extends Model
{
    protected $table ="blog";
    protected $primaryKey = "blog_id";
    protected $fillable = ['blog_title','blog_published_datetime', 'blog_content'];
    public $blog_id;
    public $blog_title;
    public $blog_published_datetime;
    public $blog_views;
    public $blog_content;
    public $blog_status;
    public $admin_id;
    public $timestamps = false;

    // public function collection()
    // {
    //    $blog = DB::where('blog_status','=','1')->sortByDesc("blog_id")->get();
    //    return $blog;
    // }
    public function view_all($search)
    {
    	$blog = DB::table($this->table)->join('admin','admin.admin_id','=','blog.admin_id')->where([
            ['blog_title','like',"%$search%"],
            ['blog.admin_id', $this->admin_id]
        ])->paginate(5);
        // dd($blog);
    	return $blog;
    }
    public function get_time()
    {
    	$dt = Carbon::now('Asia/Ho_Chi_Minh');
    	return $dt;
    }

    public function process_insert()
    {
    	DB::table($this->table)->insert([
    		'blog_title'=>$this->blog_title,
    		'blog_published_datetime'=>$this->blog_published_datetime,
            'blog_content'=>$this->blog_content,
    		'admin_id'=>$this->admin_id
    	]);
    }

    public function view_update($search)
    {
    	$blog = DB::table($this->table)->where('blog_id',$this->blog_id)->get()->first();
    	return $blog;
    }

    public function process_update()
    {
        DB::table($this->table)->where('blog_id',$this->blog_id)->update([
            'blog_title'=>$this->blog_title,
            'blog_published_datetime'=>$this->blog_published_datetime,
            'blog_status'=>$this->blog_status,
            'blog_content'=>$this->blog_content,
            'admin_id'=>$this->admin_id
        ]);
    }
    public function process_delete()
    {
        DB::table($this->table)->where('blog_id',$this->blog_id)->delete();
    }

}
