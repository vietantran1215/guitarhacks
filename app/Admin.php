<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class Admin extends Model
{
    protected $table = "admin";
    public $admin_id;
    public $admin_full_name;
    public $admin_email;
    public $admin_password;
    public $img;

    public function view_all($search)
    {
    	$array_admin = DB::table($this->table)->where('admin_full_name','like',"%$search%")->paginate(5);
        // dd($array_admin);
    	return $array_admin;
    }
    public function process_insert()
    {
    	DB::table($this->table)->where('admin_id',$this->admin_id)
        ->insert([
            'admin_full_name'       => $this->admin_full_name,
            'admin_email' => $this->admin_email,
            'admin_password'    => $this->admin_password,
            'img'=>$this->img
        ]);

    }
    public function view_one()
    {
    	$array_admin = DB::table($this->table)->where('admin_id',$this->admin_id)->get()->first();
        //dd($array_admin);
        return $array_admin;
    }
    public function process_upload()
    {
    	DB::table($this->table)->where('admin_id',$this->admin_id)
        ->update([
            'admin_full_name'       => $this->admin_full_name,
            'admin_email' => $this->admin_email,
            'admin_password'    => $this->admin_password,
            'img'    => $this->img
        ]);
    }
    public function process_delete()
    {
    	DB::table($this->table)->where('admin_id',$this->admin_id)
        ->delete();
    }
    
    public function process_login($value='')
    {
        $amdin = DB::table($this->table)->where('admin_email',$this->admin_email)->where('admin_password',$this->admin_password)->first();
        return $amdin;
    }
}
