<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Bill;
use Exception;
class BillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $bill = new Bill();
        // $array_bill = $bill->view_all();
        $bill = Bill::all();
        return response()->json($bill);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $bill = Bill::create($request->all());
            return response()->json($bill);
            
        } catch (Exception $e) {
            return response()->json($e);
        }
        
        // return response()->json($bill, Response::HTTP_OK);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($bill_id)
    {
        $bill = Bill::find($bill_id);
        return response()->json($bill);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $bill_id)
    {
        try {
            $bill = Bill::find($bill_id);
            $result = $bill->update($request->all()); 
            return response()->json($bill);
        } catch (Exception $e) {
            return response()->json($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($bill_id)
    {
        try {
            $bill = Bill::find($bill_id)->delete();
            return response()->json($bill);
        } catch (Exception $e) {
            return response()->json($e);
        }
    }
}
