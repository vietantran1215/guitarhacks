<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Blog;
use Illuminate\Support\collection;
use Exception;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
            $search = $request->get('search');
            $blog = Blog::where('blog_status','=','1')->orderBy('blog_published_datetime', 'desc')->where('blog_title','like',"%$search%")
            ->get();
            return response()->json($blog);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $blog = Blog::create($request->all());
            return response()->json($blog);
            
        } catch (Exception $e) {
            return response()->json(false);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($blog_id)
    {
        return Blog::find($blog_id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $blog_id)
    {
        try {
            $blog = Blog::find($blog_id)->update($request->all());
            return response()->json($blog);
            
        } catch (Exception $e) {
            return response()->json(false);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($blog_id)
    {
        try {
            $blog = Blog::find($blog_id)->delete();
            return response()->json(true);

        } catch (Exception $e) {
            return response()->json(false);
        }
    }
}
