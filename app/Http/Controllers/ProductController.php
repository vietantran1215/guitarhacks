<?php

namespace App\Http\Controllers;

use Request;
use App\Product;

class ProductController extends Controller
{
    public function view_all(){
        $search       = Request::get('search');
        $product = new Product();
        $array_product = $product->view_all($search);
        return view('guitarhacks_course.product.view_all', ['array_product' => $array_product,'search'=>$search]);
    }
    public function view_insert()
    {   $search       = Request::get('search');
    	return view('guitarhacks_course.product.view_insert',['search'=>$search]);
    }
    public function process_insert()
    {
    	$product = new Product();
    	$product->product_name			= Request::get('product_name');
    	$product->product_description	= Request::get('product_description');
    	$product->product_price 		= Request::get('product_price');
        $product->process_insert();
        return redirect()->route('product.view_all');
    }

    public function view_update($id)
    {
        $search = Request::get('search');
        $product = new Product();
        $product->product_id = $id;
        $product = $product->view_update();
        return view('guitarhacks_course.product.view_update', ['product' => $product,'search'=>$search]);
    }

    public function process_delete($id)
    {
    	$product = new Product();
    	$product->product_id = $id;
    	$product->process_delete();
    	return redirect()->route('product.view_all');
    }

    // public function view_upload($id)
    // {
    //     $search = Request::get('search');
    //     $product = new Product();
    //     $product->product_id = $id;
    //     $product = $product->view_update();
    //     return view('guitarhacks_course.product.view_upload', ['product' => $product,'search'=>$search]);
    // }

    public function process_update()
    {
        $product = new Product();
        $product->product_id = Request::get('product_id');
        // dd( $product->product_id );
        $product->product_name          = Request::get('product_name');
        $product->product_description   = Request::get('product_description');
        // dd( $product->product_description );
        $product->product_price         = Request::get('product_price');
        $product->process_update();
        return redirect()->route('product.view_all');
    }
}
