<?php

namespace App\Http\Controllers;

use Request;
use App\Bill;
use App\Product;

class BillController extends Controller
{
    public function view_all()
    {
        $search = Request::get('search');
    	$bill = new Bill();
    	$array_bill = $bill->view_all($search);
    	return view('guitarhacks_course.bill.view_all',['array_bill'=>$array_bill, 'search'=>$search]);
    }

    public function view_insert()
    {
    	$product = new Product();
    	$array_product = $product->view_all();
    	
    	return view('guitarhacks_course.bill.view_insert',['array_product'=>$array_product]);
    }

    public function process_insert()
    {
    	$bill = new Bill();
  
    	$bill->bill_customer_name = Request::get('bill_customer_name');
    	$bill->bill_customer_phone_number = Request::get('bill_customer_phone_number');
    	$bill->bill_product_id = Request::get('bill_product_id');
    	$bill->bill_status = Request::get('bill_status');
    	$bill->process_insert();
    	//dd($bill);
    	return redirect()->route('bill.view_all');
    }

    public function view_update($id)
    {
        $product = new Product();
        $array_product = $product->view_all();

    	$bill = new Bill();
    	$bill->bill_id = $id;
    	$bill = $bill->view_one();
    	return view('guitarhacks_course.bill.view_update',['bill'=>$bill],['array_product'=>$array_product]);
    }

    public function process_update()
    {
        $bill = new Bill();
        $bill->bill_id                      = Request::get('bill_id');
        $bill->bill_customer_name           = Request::get('bill_customer_name');
        $bill->bill_customer_phone_number   = Request::get('bill_customer_phone_number');
        $bill->bill_status                  = Request::get('bill_status');
        $bill->bill_product_id              = Request::get('bill_product_id');
        $bill->process_update();
        return redirect()->route('bill.view_all');

    }
    public function process_delete($id)
    {
        $bill = new Bill();
        $bill->bill_id = $id;
        $bill->process_delete();
        return redirect()->route('bill.view_all');
    }
}
