<?php

namespace App\Http\Controllers;
use Request;
use App\Admin;
use Exception;
use Storage;

class AdminController extends Controller
{
    public function view_all()
    {
        $search = Request::get('search');
        $admin = new Admin();
        $array_admin = $admin->view_all($search);
        return view('guitarhacks_course.admin.view_all',['array_admin'=>$array_admin,'search'=>$search]);
    }
    public function view_insert()
    {
    	return view('guitarhacks_course.admin.view_insert');
    }
    public function process_insert()
    {
    	$admin = new Admin();
    	$admin->admin_full_name 	= Request::get('admin_full_name');
    	$admin->admin_email 		= Request::get('admin_email');
    	$admin->admin_password 		= Request::get('admin_password');
        $admin->img                 = Request::get('img');
    	//dd($admin);
    	$admin->process_insert();
    	return redirect()->route('admin.view_all');
    }
    
    public function process_update()
    {
         $admin = new Admin();
         // $admin->admin_id            = Request::get('admin_id');
         // $admin->admin_full_name    = Request::get('admin_full_name');
         // $admin->admin_email         = Request::get('admin_email');
         // $admin->admin_password      = Request::get('admin_password');
        
        if (Request::hasfile('img')) {
            $img = Request::file('img');
            $file_img = $img->getClientOriginalName('img');

            $img->move(public_path('img/photos'), $file_img);
            
            $admin->img = $file_img;
            // dd($file_img);
            $admin->process_insert();
            return redirect()->route('admin.view_all');
        }else{
            echo "chưa có file";
        }
    }
    public function process_delete($id)
    {
        $admin = new Admin();
        $admin->admin_id = $id;
        $admin->process_delete();
        return redirect()->route('admin.view_all');
    }
    public function view_login($value='')
    {
        return view('guitarhacks_course.admin.view_login');
    }
    public function process_login()
    {
        $admin = new Admin();
        $admin->admin_full_name = Request::get('admin_full_name');
        $admin->img = Request::get('img');
        $admin->admin_email = Request::get('admin_email');
        $admin->admin_password  = Request::get('admin_password');

        try {
            $admin = $admin->process_login();
            Request::session()->put('admin_full_name',$admin->admin_full_name);
            Request::session()->put('img',$admin->img);
            Request::session()->put('admin_id',$admin->admin_id);
            Request::session()->put('admin_email',$admin->admin_email);
            return redirect()->route('admin.view_all');

        } catch (Exception $e) {
            return redirect()->route('view_login')->with('error',"Đăng nhập sai rồi");
        }
    }
    public function logout()
    {
        Request::session()->flush();
        Request::session()->forget('admin_full_name','admin_email','admin_password');
        return redirect()->route('view_login')->with('error','đăng nhập lại đi');
    }
    public function view_upload($id)
    {
        $search = Request::get('search');
        $admin = new Admin();
        $admin->admin_id = $id;
        $admin = $admin->view_one();   
        return view('guitarhacks_course.admin.view_upload',['admin'=>$admin,'search'=>$search]);
    }
    public function process_upload()
    {
         $admin = new Admin();
         $admin->admin_id            = Request::get('admin_id');
         $admin->admin_full_name    = Request::get('admin_full_name');
         $admin->admin_email         = Request::get('admin_email');
         $admin->admin_password      = Request::get('admin_password');
        
        if (Request::hasfile('img')) {
            $img = Request::file('img');
            $file_img = $img->getClientOriginalName('img');

            $img->move(public_path('admin/img/admin_img'), $file_img);
            
            $admin->img = $file_img;
            // dd($file_img);
            $admin->process_upload();
            return redirect()->route('admin.view_all');
        }else{
            echo "chưa có file";
        }

    }
    
    public function view_profile($id)
    {
        $search = Request::get('search');
        $admin = new Admin();
        $admin->admin_id = $id;
        $admin = $admin->view_one();
        return view('guitarhacks_course.admin.view_profile',['admin'=>$admin,'search'=>$search]);
    }
    public function button($value='')
    {
        $gclass = new Gclass();
        $gclass->gclass_id = $id;
        $gclass->button();
    }
}
