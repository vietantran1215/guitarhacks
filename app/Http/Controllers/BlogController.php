<?php

namespace App\Http\Controllers;

use Request;
use App\Blog;
use App\Admin;
use Session;

class BlogController 
{
    public function view_all()
    {
        $search = Request::get('search');
        $blog = new Blog;
        $blog->admin_id = Session::get('admin_id');
        // dd($blog->admin_id);
        $array_blog = $blog->view_all($search);
        return view('guitarhacks_course.blog.view_all',['array_blog'=>$array_blog, 'search'=>$search]);
    }
    public function view_insert()
    {
        // $admin = new Admin();
        // dd($admin->admin_full_name = Request::get('admin_full_name'));
        $search = Request::get('search');
        $blog = new Blog;
        $dt = $blog->get_time();
        return view('guitarhacks_course.blog.view_insert',['dt'=>$dt, 'search'=>$search]);
    }

    public function process_insert()
    {
       $blog = new Blog;
       $blog->blog_title = Request::get('blog_title');
       $blog->blog_views = Request::get('blog_views');
       $blog->blog_published_datetime = Request::get('blog_published_datetime');
       $blog->blog_content = Request::get('blog_content');
       $blog->admin_id = Request::get('admin_id');
       $blog->process_insert();
       return redirect()->route('blog.view_all');
    }

    public function view_update($id)
    {
        $search = Request::get('search');
        $blog = new Blog;
        $blog->blog_id = $id;
        $dt = $blog->get_time();
        $blog= $blog->view_update($search);
        return view('guitarhacks_course.blog.view_update',['blog'=>$blog,'search'=>$search,'dt'=>$dt]);
    }

    public function process_update()
    {
        $blog = new Blog;
        $blog->blog_id = Request::get('blog_id');
        $blog->blog_title = Request::get('blog_title');
        $blog->blog_views = Request::get('blog_views');
        $blog->blog_published_datetime = Request::get('blog_published_datetime');
        $blog->blog_status = Request::get('blog_status');
        $blog->blog_content = Request::get('blog_content');
        $blog->admin_id = Request::get('admin_id');
        $blog->process_update();
        return redirect()->route('blog.view_all');
    }
    public function process_delete($id)
    {
        $blog = new Blog;
        $blog->blog_id = $id;
        $blog->process_delete();
        return redirect()->route('blog.view_all');
    }
}
