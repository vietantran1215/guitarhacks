<?php

namespace App\Http\Controllers;

use Request;
use App\Timetable;

class TimetableController extends Controller
{
    public function view_all(){
        $search = Request::get('search');
        $timetable       = new Timetable();
        $array_timetable = $timetable->view_all();
        return view('guitarhacks_course.timetable.view_all', ['array_timetable' => $array_timetable,'search'=>$search]);
    }
    public function process_insert(){
        $timetable                 = new Timetable();
        $timetable->timetable_day  = Request::get('timetable_day');
        $timetable->timetable_time = Request::get('timetable_time');
        $timetable->process_insert();
        return redirect()->route('timetable.view_all');
    }
    public function process_delete($id){
        $timetable               = new Timetable();
        $timetable->timetable_id = $id;
        $timetable->process_delete();
        return redirect()->route('timetable.view_all');
    }
}
