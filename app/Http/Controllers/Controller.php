<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Request;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

use App\Student;
use App\Gclass;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // public function view_send_mail($value='')
    // {
    //     return view('guitarhacks.student.view_all');
    // }
	public function send_mail($student_email, $gclass_id)
	{
        $gclass = new Gclass();
        $gclass->gclass_id = $gclass_id;
        $gclass = $gclass->view_one();
		$mail = new PHPMailer(true);

    try {
        //Server settings
        $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
        $mail->isSMTP();                                            // Send using SMTP
        $mail->Host       = gethostbyname('smtp.gmail.com');                    // Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $mail->Username   = 'thanhhuyenn1602@gmail.com';                     // SMTP username
        $mail->Password   = 'Huyen1602';                               // SMTP password
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
        $mail->Port       = 587;        
        $mail->SMTPOptions = array(
            'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
            )
        );                            // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

        //Recipients
        $email = Request::get('email');
        $mail->setFrom('thanhhuyenn1602@gmail.com', 'Huyen');
        // $emails = implode(' , ',array_values($email));
        // $mail->addAddress('0333468620@vtext.com', 'Huyen');
        $mail->addAddress("$student_email") ; // Add a recipient
        // print_r($mail);
        // $mail->addAddress('ellen@example.com');               // Name is optional
        // $mail->addReplyTo('info@example.com', 'Information');
        // $mail->addCC('cc@example.com');
        // $mail->addBCC('bcc@example.com');

        // Attachments
        // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
        
        // Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = '';
        $mail->Body    = GclassController::view($gclass_id);
        $mail->AltBody = 'toang';

        $mail->send();
        echo 'Message has been sent';
    } catch (Exception $e) {
        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    }
    	}

}
// <table>
//             <tr>
//                 <td>Lớp</td>
//                 <td>{{$gclass->gclass_name}}</td>
//             </tr>
//             <tr>
//                 <td>Ngày khai giảng</td>
//                 <td>{{$gclass->gclass_start_date}}</td>
//             </tr>
//             <tr>
//                 <td>Khóa</td>
//                 <td>{{$gclass->course_title}}</td>
//             </tr>
//             <tr>
//                 <td>Số buổi</td>
//                 <td>{{$gclass->gclass_lesson_left}}</td>
//             </tr>
//             <tr>
//                 <td>Thời gian</td>
//                 <td>{{$gclass->timetable_day}}</td>
//             </tr>
//         </table