<?php

namespace App\Http\Controllers;

use Request;
use App\Gclass;
use App\Course;
use App\Timetable;
use App\Student;
use DB;

class GclassController extends Controller
{
    public function view_all(){
    
        $search       = Request::get('search');
        $today        = date('Y-m-d');
        $gclass       = new gclass();
        $gclass->update_all($today);
        $array_gclass = $gclass->view_all($search);

        return view('guitarhacks_course.gclass.view_all', ['array_gclass' => $array_gclass,'search'=>$search]);
    }

    public  static function view($id)
    {
        $search = Request::get('search');
        $today        = date('Y-m-d');
        $gclass       = new gclass();
        $gclass->gclass_id = $id;
        $gclass->update_all($today);
        $gclass = $gclass->view($search);
        // dd($array_gclass);
        return view('guitarhacks_course.gclass.view',['gclass'=>$gclass,'search'=>$search]);
    }
    public function view_all_to_course($id){
        $search       = Request::get('search');
        $course                   = new Course();
        $course->course_id        = $id;
        $course                   = $course->view_one();


        $gclass                   = new Gclass();
        $gclass->gclass_course_id = $id;
        $array_gclass             = $gclass->view_all_to_course($search);

        return view('guitarhacks_course.course.view_course_detail', [
            'array_gclass' => $array_gclass,
            'course'       => $course,
            'search'       => $search,
            // 'gclass'=>$gclass
        ]);
    }
    public function update_all(){
        $today = date('Y-m-d');
        $gclass = new Gclass();
        $gclass->update_all($today);

        return redirect()->route('gclass.view_all');
    }
    public function view_insert(){
        $search       = Request::get('search');
        $course          = new Course();
        // $array_course    = $course->view();
        $array_course = DB::table('course')->get();
        // dd($array_course);
        $timetable       = new Timetable();
        $array_timetable = $timetable->view_all();

        return view('guitarhacks_course.gclass.view_insert', [
            'array_course' => $array_course,
            'array_timetable' => $array_timetable,
            'search'=>$search
        ]);
    }
    public function view_insert_to_course($id){
        $search       = Request::get('search');
        $course            = new Course();
        $course->course_id = $id;
        $course            = $course->view_one();

        $timetable         = new Timetable();
        $array_timetable   = $timetable->view_all();

        return view('guitarhacks_course.gclass.view_insert_to_course',[
            'course'          => $course,
            'array_timetable' => $array_timetable,
            'search'=>$search
        ]);
    }
    public function process_insert(){
        $gclass                      = new Gclass();
        $gclass->gclass_name         = Request::get('gclass_name');
        $gclass->gclass_start_date   = Request::get('gclass_start_date');
        $gclass->gclass_course_id    = Request::get('gclass_course_id');
        $gclass->gclass_timetable_id = Request::get('gclass_timetable_id');

        // Get course duration and generate ending date automatically
        $course                      = new Course();
        $course                      = $course->view_one_to_gclass($gclass->gclass_course_id);
        $course_duration             = $course->course_duration * 7 - 7;
        if(Request::get('gclass_end_date') !== null){
            $gclass->gclass_end_date = Request::get('gclass_end_date');
        }
        else{
            $gclass->gclass_end_date  = date('Y-m-d', strtotime($gclass->gclass_start_date.' + '.$course_duration.' days'));
        }
        $gclass->gclass_lesson_left = $course->course_duration;
        $gclass->process_insert();

        return redirect()->route('gclass.view_all');
    }
    public function view_update($id){

        $search       = Request::get('search');
        $gclass            = new Gclass();
        $gclass->gclass_id = $id;
        $gclass            = $gclass->view_one($search);

        // $course            = new Course();
        $array_course      = DB::table('course')->get();

        $timetable         = new Timetable();
        $array_timetable   = $timetable->view_all($search);

        return view('guitarhacks_course.gclass.view_update', [
            'gclass'          => $gclass,
            'array_course'    => $array_course,
            'array_timetable' =>$array_timetable,
            'search'=>$search
        ]);
    }
    public function process_update(){
        $gclass                    = new Gclass();
        $gclass->gclass_id         = Request::get('gclass_id');
        $gclass->gclass_name       = Request::get('gclass_name');
        $gclass->gclass_start_date = Request::get('gclass_start_date');
        $gclass->gclass_course_id  = Request::get('gclass_course_id');

        $course = new Course();
        $course = $course->view_one_to_gclass($gclass->gclass_course_id);

        $course_duration = $course->course_duration * 7 - 7;
        if(Request::get('gclass_end_date') !== null){
            $gclass->gclass_end_date = Request::get('gclass_end_date');
        }
        else{
            $gclass->gclass_end_date  = date('Y-m-d', strtotime($gclass->gclass_start_date.' + '.$course_duration.' days'));
        }

        $gclass->gclass_lesson_left = $course->course_duration;

        $gclass->process_update();
        return redirect()->route('gclass.view_all');
    }
    public function process_delete($id){
        $student                   = new Student();
        $student->student_class_id = $id;
        $student->process_delete_by_class();

        $gclass            = new Gclass();
        $gclass->gclass_id = $id;
        $gclass->process_delete();
        return redirect()->route('gclass.view_all');
    }

    public function button($id)
    {
        $gclass = new Gclass();
        $gclass->gclass_course_id = $id;
        // dd( $gclass->$gclass_name = $gclass_name);
        // $gclass->$gclass_name = $gclass_name;
        $gclass = $gclass->button();
        return redirect()->route('gclass.view_all');
    }
}
