<?php

namespace App\Http\Controllers;

use Request;
use App\Student;
use App\Gclass;
use App\Registration;
use App\Course;
use DB;

class StudentController extends Controller
{
    public function view_all(){
        $search = Request::get('search');
        $student       = new Student();
        $array_student = $student->view_all($search);
        
        return view('guitarhacks_course.student.view_all', [
            'array_student' => $array_student,'search'=>$search
        ]);
    }
    public function view_all_to_gclass($id){
        $search       = Request::get('search');
        $gclass            = new Gclass();
        $gclass->gclass_id = $id;
        $gclass            = $gclass->view($search);

        // $gclass = new Gclass()
        // $array_gclass = $gclass->view_all($search);
        // dd($array_gclass);

        $registration                         = new Registration();
        $registration->registration_course_id = $gclass->gclass_course_id;

        // $array_gclass = DB::table('gclass')->join('course','course.course_id','=','gclass.gclass_course_id')
        //                                    ->where('course.course_id',$gclass_course_id)
        //                                    ->get();
                                        
        $student                    = new Student();
        $student->student_gclass_id = $id;
        $array_student              = $student->view_all_to_gclass();
        $array_unset_student        = $student->view_all_unset_to_gclass($registration->registration_course_id);


        return view('guitarhacks_course.gclass.view_gclass_detail', [
            'array_student'       => $array_student,
            'array_unset_student' => $array_unset_student,
            'gclass'              => $gclass,
            'search'              =>$search
        ]);
    }
    
    // public function process_insert($registration_id){
    //     $student                          = new Student();
    //     $student->student_registration_id = $registration_id;
    //     $student->process_insert();
    //
    //     return redirect()->route('student.view_all_to_gclass');
    // }
    public function process_set_gclass($gclass_id, $student_id, $student_email){
        $student                    = new Student();
        $student->student_gclass_id = $gclass_id;
        $student->student_id        = $student_id;
        $student->process_set_gclass();

        $student                       = $student->view_one();
        $registration                  = new Registration();
        $registration->registration_id = $student->student_registration_id;
        $registration->process_set_gclass();

        Controller::send_mail($student_email, $gclass_id);

        return redirect()->route('student.view_all_to_gclass', ['id' => $gclass_id]);
    }
    public function process_unset_gclass($gclass_id, $student_id){
        $student             = new Student();
        $student->student_id = $student_id;
        $student->process_unset_gclass();

        $student                       = $student->view_one();
        $registration                  = new Registration();
        $registration->registration_id = $student->student_registration_id;
        $registration->process_unset_gclass();

        return redirect()->route('student.view_all_to_gclass', ['id' => $gclass_id]);
    }

    public function process_delete($student_id)
    {
        $student = new Student();
        $student->student_id = $student_id;
        $student->process_delete();
        return redirect()->route('student.view_all');
    }
}
