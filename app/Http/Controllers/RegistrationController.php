<?php

namespace App\Http\Controllers;

use Request;
use App\Registration;
use App\Course;
use App\Student;

class RegistrationController extends Controller
{
    public function view_all(){
        $search = Request::get('search');
        $registration = new Registration();
        $array_registration = $registration->view_all($search);

        return view('guitarhacks_course.registration.view_all', [
            'array_registration' => $array_registration,
            'search' =>$search
        ]);
    }
    public function view_insert(){
        $search = Request::get('search');
        $course       = new Course();
        $array_course = $course->view_all($search);
        // dd($array_course);
        return view('guitarhacks_course.registration.view_insert', [
            'array_course' => $array_course,
            'search'=>$search
        ]);
    }
    public function process_insert(){
        $registration                                     = new Registration();
        $registration->registration_id                    = Request::get('registration_id');
        $registration->registration_student_full_name     = Request::get('registration_student_full_name');
        $registration->registration_student_phone_number  = Request::get('registration_student_phone_number');
        $registration->registration_student_date_of_birth = Request::get('registration_student_date_of_birth');
        $registration->registration_student_email         = Request::get('registration_student_email');
        $registration->registration_course_id             = Request::get('registration_course_id');
        // dd($registration->registration_student_email);
        $course            = new Course();
        $course->course_id = Request::get('registration_course_id');
        $course            = $course->view_one();

        $registration->registration_lesson_left = $course->course_duration;
        $registration->process_insert();

        $registration_id = $registration->view_latest_one();

        $student                          = new Student();
        $student->student_registration_id = $registration_id;
        $student->process_insert();

        return redirect()->route('registration.view_all');
    }
    public function process_delete($id){
        // $student = new Student();
        // $student->student_registration_id = $id;
        // $student->process_delete();
        // return redirect()->route('registration.view_all');
        $registration                  = new Registration();
        $registration->registration_id = $id;
        $registration->process_delete();
        return redirect()->route('registration.view_all');
    }
}
