<?php

namespace App\Http\Controllers;

use Request;
use App\Course;
use App\Gclass;

class CourseController extends Controller
{
    public function view_all(){
        $search       = Request::get('search');
        $course       = new Course();
        $array_course = $course->view_all($search);
        return view('guitarhacks_course.course.view_all', ['array_course' => $array_course,'search'=>$search]);
    }
    public function view_insert(){
        $search       = Request::get('search');
        return view('guitarhacks_course.course.view_insert',['search'=>$search]);
    }
    public function process_insert(){
        $course                     = new Course();
        $course->course_title       = Request::get('course_title');
        $course->course_description = Request::get('course_description');
        $course->course_duration    = Request::get('course_duration');
        $course->course_price       = Request::get('course_price');
        $course->process_update();
        return redirect()->route('course.view_all');
    }
    public function view_update($id){
        $search       = Request::get('search');
        $course            = new Course();
        $course->course_id = $id;
        $course            = $course->view_one();
        return view('guitarhacks_course.course.view_update', ['course' => $course,'search'=>$search]);
    }
    public function process_update(){
        $course = new Course();
        $course->course_id          = Request::get('course_id');
        $course->course_title       = Request::get('course_title');
        $course->course_description = Request::get('course_description');
        $course->course_duration    = Request::get('course_duration');
        $course->course_price       = Request::get('course_price');
        $course->process_update();
        return redirect()->route('course.view_all');
    }
    public function process_delete($id){
        $gclass = new Gclass();
        $gclass->gclass_course_id = $id;
        $gclass_count = count($gclass->view_all_to_course());
        if($gclass_count == 0){
            $course = new Course();
            $course->course_id = $id;
            $course->process_delete();
            return redirect()->route('course.view_all');
        }
    }
}
