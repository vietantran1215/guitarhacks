<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Registration extends Model
{
    protected $table = "registration";
    protected $primaryKey = "registration_id";
    protected $fillable = ['registration_student_full_name','registration_student_phone_number','registration_student_email','registration_course_id','registration_student_date_of_birth','registration_date','registration_status','registration_lesson_left'];
    public $registration_id; //*
    public $registration_student_full_name;
    public $registration_student_phone_number;
    public $registration_student_date_of_birth;
    public $registration_date; //*
    public $registration_status; //*
    public $registration_lesson_left; //course_duration
    public $registration_course_id;
    public $registration_student_email;
    public $timestamps = false;
    
    public function view_all($search){
        $array_registration = DB::table('course')->join('registration','course.course_id','=','registration.registration_course_id')->join('student','registration.registration_id','=','student.student_registration_id')->where('registration_student_full_name','like',"%$search%")->paginate(7);
         // $array_registration = DB::connection($this->connection)
         //                        ->table($this->table)
         //                        ->join('course', 'course.course_id', '=', $this->table.'.registration_course_id')
         //                        ->get();
        return $array_registration;

    }
    // public function view_all_to_gclass(){
    //     $array_registration = DB::connection($this->connection)
    //                             ->table($this->table)
    //                             ->join('course', 'course.course_id', '=', $this->table.'.registration_course_id')
    //                             ->where([
    //                                 [$this->table.'.registration_course_id', '=', $this->registration_course_id]
    //                             ])
    //                             ->orderBy('registration_status', 'asc')
    //                             ->get();
    //     return $array_registration;
    // }
    public function process_insert(){
        DB::table($this->table)
            ->insert([
                'registration_student_full_name'     => $this->registration_student_full_name,
                'registration_student_phone_number'  => $this->registration_student_phone_number,
                'registration_student_date_of_birth' => $this->registration_student_date_of_birth,
                'registration_lesson_left'           => $this->registration_lesson_left,
                'registration_course_id'             => $this->registration_course_id,
                'registration_student_email'         => $this->registration_student_email
            ]);
    }
    public function process_delete(){

        DB::table($this->table)
            ->where('registration_id', $this->registration_id)
            ->delete();
    }
    public function view_latest_one(){
        $registration_id = DB::table($this->table)
                            ->max('registration_id');
        return $registration_id;
    }
    public function process_set_gclass(){
        DB::table($this->table)
            ->where('registration_id', '=', $this->registration_id)
            ->update([
                'registration_status' => 1
            ]);
    }
    public function process_unset_gclass(){
        DB::table($this->table)
            ->where('registration_id', '=', $this->registration_id)
            ->update([
                'registration_status' => 0
            ]);
    }
}
