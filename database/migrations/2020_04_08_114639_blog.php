<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Blog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::connection('mysql')->create('blog', function (Blueprint $table) {
            $table->increments('blog_id');
            $table->string('blog_title');
            $table->datetime('blog_published_datetime');
            $table->unsignedInteger('blog_views')->default(0);
            $table->unsignedTinyInteger('blog_status')->default(0);
            $table->string('content');
            $table->unsignedInteger('admin_id');
            $table->foreign('admin_id')->references('admin_id')->on('admin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog');
    }
}
