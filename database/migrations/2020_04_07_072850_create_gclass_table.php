<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGclassTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('gclass', function (Blueprint $table) {
            $table->increments('gclass_id');
            $table->string('gclass_name')->unique();
            $table->date('gclass_start_date');
            $table->date('gclass_end_date');
            $table->unsignedInteger('gclass_lesson_left');
            $table->tinyInteger('gclass_status')->default(0);
            $table->unsignedInteger('gclass_course_id');
            $table->unsignedInteger('gclass_timetable_id');
            $table->foreign('gclass_course_id')->references('course_id')->on('course');
            $table->foreign('gclass_timetable_id')->references('timetable_id')->on('timetable');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gclass');
    }
}
