<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('registration', function (Blueprint $table) {
            $table->increments('registration_id');
            $table->string('registration_student_full_name');
            $table->string('registration_student_phone_number');
            $table->date('registration_student_date_of_birth');
            $table->date('registration_date')->useCurrent();
            $table->tinyInteger('registration_status')->default(0);
            $table->unsignedInteger('registration_lesson_left');
            $table->unsignedInteger('registration_course_id');
            $table->foreign('registration_course_id')->references('course_id')->on('course');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regitration');
    }
}
