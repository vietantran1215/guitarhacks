<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('student', function (Blueprint $table) {
            $table->increments('student_id');
            $table->unsignedInteger('student_registration_id')->unique();
            $table->unsignedInteger('student_gclass_id')->nullable();
            $table->foreign('student_gclass_id')->references('gclass_id')->on('gclass');
            $table->foreign('student_registration_id')->references('registration_id')->on('registration');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student');
    }
}
