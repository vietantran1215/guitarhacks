<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('bill', function (Blueprint $table) {
            $table->increments('bill_id');
            $table->string('bill_customer_name');
            $table->char('bill_customer_phone_number');
            $table->unsignedTinyInteger('bill_status');
            $table->unsignedInteger('bill_product_id');
            $table->foreign('bill_product_id')->references('product_id')->on('product');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bill');
    }
}
